package fr.begel.apps.bonvoyage.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface TripDayDAO {

    /*@Transaction
    @Query("SELECT tripid as day_tripid, date as day_date, name as day_name, id as day_id, last_modified as day_last_modified FROM trip_days WHERE tripid = :tripId ORDER by date")
    fun getTripDaysWithEvents(tripId: String): LiveData<List<TripDayWithEvents>>*/

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertAllTripDays(tripDays: Collection<TripDay>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllTripDays(elements: List<TripDay>)

    @Query("SELECT * FROM trip_days")
    fun getAllTripDays(): LiveData<List<TripDay>>

    @Query("SELECT * FROM trip_days WHERE tripid = :tripId")
    fun getTripDays(tripId: String): LiveData<List<TripDay>>

    @Transaction
    @Query("SELECT * from trips where tripid = :tripId")
    fun getTripWithDays(tripId: String): LiveData<TripWithDays>

    @Delete
    fun deleteTripDay(tripDay: TripDay)

    @Query("DELETE FROM trip_days WHERE tripid = :tripId")
    fun deleteAllDaysOfTrip(tripId: String)

    @Transaction
    @Query("SELECT * FROM trip_days WHERE tripid = :tripId")
    fun getTripDaysWithEventsId(tripId: String): LiveData<List<TripDayWithEventsId>>
 }