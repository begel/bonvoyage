package fr.begel.apps.bonvoyage.ui

import android.os.Bundle
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.routing.*
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.MyItemClickListener
import fr.begel.apps.bonvoyage.listeners.MyItemLongClickListener
import fr.begel.apps.bonvoyage.persistence.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


const val SUMMARY_TRIP_ID = "fr.begel.apps.bonvoyage.ui.summary_trip_id"
const val DAY_ID = "fr.begel.apps.bonvoyage.ui.day_id"


class EventsOfTrip : Fragment(),
        MyItemLongClickListener, MyItemClickListener,
        RouteListener.AfterCalculateRouteListener
{

    private var trip: Trip? = null
    var toScroll: String = ""
    private var days : List<TripDayWithEvents> = emptyList()
    private var allEventsMap: HashMap<String, Event> = HashMap()
    private var daysWithEventsId: List<TripDayWithEventsId> = emptyList()
    private var mDataSet: ArrayList<Any> = arrayListOf()
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventViewModel
    private lateinit var viewAdapter: EventAdapter
    private var mView: View? = null
    private var mRecyclerView: RecyclerView? = null
    private var mLayoutManager = LinearLayoutManager(context)
    private var mapEngine: MapEngine = MapEngine.getInstance()
    private val routeManager: RouteManager = RouteManager()
    private val routesBtwEventsToCalculate = ArrayList<RouteBtwEvents>()
    private var existingRoutesBtwEvents : ArrayList<RouteBtwEvents>? = null
    private var toInsertRoutesBtwEvents : ArrayList<RouteBtwEvents> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var tripId: String? = null
        arguments?.let {
            tripId = it.getString(TRIP_ID)!!
            toScroll = it.getString(DAY_ID) ?: ""
        }
        Log.d("Help", "Init toScroll = $toScroll")
        viewAdapter = EventAdapter()
        viewAdapter.setOnItemClickListener(this)
        viewAdapter.setOnItemLongClickListener(this)

        viewModelFactory = Injection.provideViewModelFactory(context!!)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)
        viewModel.getTripById(tripId!!).observe (
                { -> this.lifecycle},
                {trip: Trip? ->
                    this.trip = trip}
        )
        trip = viewModel.getTripById(tripId!!).value

        viewModel.getTripDaysWithEventsId(tripId!!).observe({-> this.lifecycle}){it ->
            Log.d("Help", "getTripDaysWithEventsId observed with ${it?.size} elements")
            daysWithEventsId = it ?: emptyList()
            updateDataSet()
        }
        viewModel.getEventsOfTrip(tripId!!).observe (
                { -> this.lifecycle},
                {events: List<Event>? ->
                    Log.d("Help", "getEventsOfTrip observed with ${events?.size} elements")
                    if (events != null){
                        allEventsMap = HashMap()
                        for (event in events) {
                            Log.d("Help", "Put ${event.name} in HashMap")
                            allEventsMap.put(event.id, event)
                        }
                        updateDataSet()
                    }
                }
        )

        viewModel.getRoutesByTripId(tripId!!).observe({this.lifecycle}) {
            routes ->
            Log.d("Help", "viewModel observe routes, size=${routes?.size}")
            existingRoutesBtwEvents = routes as ArrayList<RouteBtwEvents>
            updateDataSet()
        }
        mapEngine.init(context){}
    }

    fun updateDataSet() {
        Log.d("Help", "DataSet: ${mDataSet}")
        val position = (mDataSet.map { e ->
            (e as? TripDay)?.dayId
        }).indexOf(toScroll)
        Log.d("Help", "Position to Scroll: $position, dayId: $toScroll")
        mLayoutManager.scrollToPositionWithOffset(position, 0)
        Log.d("Help", "HashMap size : ${allEventsMap.size} - Days size : ${daysWithEventsId.size}")
        if (daysWithEventsId.isNotEmpty() and allEventsMap.isNotEmpty() and (existingRoutesBtwEvents != null)){
            Log.d("Help", "UpdateDataSet not empty")
            days = OrderTripDaysWithEvents(allEventsMap, daysWithEventsId).days
            //mDataSet = flattenTripDays(days)
            mDataSet = extractElements(days)
            viewAdapter.setEvents(mDataSet)
        } else         Log.d("Help", "UpdateDataSet empty")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("Help", "onCreateView called")
        mView = inflater.inflate(R.layout.fragment_events_of_trip, container, false)
        mRecyclerView = mView!!.findViewById<RecyclerView>(R.id.events_recycler_view).apply {
            setHasFixedSize(true)
            mLayoutManager = LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = viewAdapter
        }
        Log.d("Help", "Recyclerview : ${mRecyclerView?.id}")
        val fab = mView!!.findViewById<FloatingActionButton>(R.id.event_add)
        fab.setOnClickListener { _ -> editEventActivity() }
        return mView
    }

    // Flatten the List of Trip Days into a List of Any
    private fun flattenTripDays(trip_days: List<TripDayWithEvents>): ArrayList<Any>{
        val list = arrayListOf<Any>()
        for (i in 0..trip_days.size - 1){
            list.add(trip_days[i].tripDay)
            list.addAll(trip_days[i].events)
        }
        return list
    }

    // Clicking on a Activity
    override fun onItemClick(view: View, position: Int) {
        val element = mDataSet[position]
        if (element as? Event != null){
            val intent = Intent(context, EventDetails::class.java).apply {
                putExtra(EVENT_DETAILS_ID, element.id)
                putExtra(TRIP_START_DATE, trip!!.first_date)
                putExtra(TRIP_END_DATE, trip!!.last_date)
            }
            startActivity(intent)
        }
        //Toast.makeText(view.context, event.name, Toast.LENGTH_SHORT).show()
    }

    // Long Click on a Activity
    override fun onItemLongClick(view: View, position: Int) {
        val element = mDataSet[position]
        if (element as? Event != null){
            if (element.coordinate != null){
                val co = element.coordinate

                val dialog = (TransportationModeDialogFragment
                        .newInstance("http://share.here.com/r/myLocation/${co.latitude},${co.longitude}?m="))
                dialog.show(activity!!.fragmentManager, "open_here_maps")
            }
        }
    }

    // Start EditEvent
    private fun editEventActivity() {
        val intent = Intent(context, EventEdit::class.java).apply {
            putExtra(TRIP_ID, trip!!.id)
            putExtra(TRIP_START_DATE, trip!!.first_date)
            putExtra(TRIP_END_DATE, trip!!.last_date)
        }
        startActivity(intent)
    }

    fun goToUrl(url: String){
        val uriUrl = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(intent)
    }

    companion object {
        @JvmStatic
        fun newInstance(tripId: String, toScroll: String) =
                EventsOfTrip().apply {
                    arguments = Bundle().apply {
                        putString(TRIP_ID, tripId)
                        putString(DAY_ID, toScroll)
                    }
                }
    }

    private fun isRouteOf(event1: Event, event2: Event, routeBtwEvents: RouteBtwEvents): Boolean{
        return ((coordinateEnd(event1) == routeBtwEvents.coordinateStart)
                and (event2.coordinate == routeBtwEvents.coordinateEnd))
    }

    private fun coordinateEnd(event: Event): GeoCoordinate? {
        return event.coordinate_end ?: event.coordinate
    }

    private fun extractElements(trip_days: List<TripDayWithEvents>): ArrayList<Any>{
        val allItems = ArrayList<Any>()
        val routes = ArrayList<Pair<GeoCoordinate, GeoCoordinate>>()
        for (day in trip_days){
            allItems.add(day.tripDay)
            for (j in 0..day.events.size - 2){
                val event1 = day.events[j]
                val event2 = day.events[j+1]
                allItems.add(event1)
                if ((coordinateEnd(event1) != null) and (event2.coordinate != null)){
                    var routeBtwEvent = existingRoutesBtwEvents!!.find{ r ->
                        isRouteOf(day.events[j], day.events[j+1], r)}
                    if (routeBtwEvent == null) {
                        routeBtwEvent = RouteBtwEvents(
                                day.tripDay.tripId, coordinateEnd(event1)!!,
                                event2.coordinate!!, "CAR"
                        )
                        routesBtwEventsToCalculate.add(routeBtwEvent)
                    }
                    allItems.add(routeBtwEvent)
                }
            }
            if (day.events.size > 0)
                allItems.add(day.events.last())
        }
        Log.d("Help", "Launch Route Calculation")
        afterCalculateRoute(null)
        return allItems
    }

    override fun afterCalculateRoute(route: Route?) {
        Log.d("Help", "Start After calculate route")
        if (route != null) {
            val routeBtwEvent = routesBtwEventsToCalculate.removeAt(0)
            routeBtwEvent.tta = route.getTta(Route.TrafficPenaltyMode.DISABLED, Route.WHOLE_ROUTE).duration
            toInsertRoutesBtwEvents.add(routeBtwEvent)
            viewAdapter.setEvents(mDataSet)
            Log.d("Help", "Assigned a route for ${route.start}-${route.destination}")
         }
        if (routesBtwEventsToCalculate.isNotEmpty()){
            val routeBtwEvents = routesBtwEventsToCalculate.first()
            val routeOptions = RouteOptions().apply {
                transportMode = RouteOptions.TransportMode.valueOf(routeBtwEvents.mode)
                routeType = RouteOptions.Type.FASTEST
            }
            val routePlan = RoutePlan()
            routePlan.routeOptions = routeOptions
            routePlan.addWaypoint(routeBtwEvents.coordinateStart)
            routePlan.addWaypoint(routeBtwEvents.coordinateEnd)
            val rl = RouteListener()
            rl.setListener(this)
            Log.d("Help", "Launch calculate route " +
                    "${routeBtwEvents.coordinateStart} - ${routeBtwEvents.coordinateEnd}")
            routeManager.calculateRoute(routePlan, rl)
        } else {
            viewModel.insertAllRoutes(toInsertRoutesBtwEvents)
            toInsertRoutesBtwEvents = ArrayList()
        }
    }
}

class RouteListener: RouteManager.Listener {
    override fun onCalculateRouteFinished(error: RouteManager.Error?, results: MutableList<RouteResult>?) {
        Log.d("Help", "Calculate route finished")
        if (error == RouteManager.Error.NONE){
            val route = results!!.first().route
            mListener!!.afterCalculateRoute(route)
        } else {
            Log.d("Help", "onCalculateRouteFinished had an error ${error.toString()}")
        }
    }
    override fun onProgress(p0: Int) {}

    private var mListener: AfterCalculateRouteListener? = null
    fun setListener(listener: AfterCalculateRouteListener) {mListener = listener}

    interface AfterCalculateRouteListener {
        fun afterCalculateRoute(route: Route?)
    }
}
