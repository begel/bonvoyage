package fr.begel.apps.bonvoyage.ui

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.*
import fr.begel.apps.bonvoyage.persistence.*

class TripAdapter(private var myDataset: List<Trip>) :
        RecyclerView.Adapter<TripAdapter.ViewHolder>() {

    private var mClickListener: MyItemClickListener? = null
    private var mLongClickListener: MyItemLongClickListener? = null
    // Provide a reference to the views for each data item_trip
    // Complex data items may need more than one view per item_trip, and
    // you provide access to all the views for a data item_trip in a view holder.
    // Each data item_trip is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View,
                     val mClickListener : MyItemClickListener?,
                     val mLongClickListener: MyItemLongClickListener?) :
            RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        val card_view = view.findViewById(R.id.item_trip_card) as CardView
        val trip_name: TextView = view.findViewById(R.id.item_trip_name) as TextView
        val trip_dates: TextView = view.findViewById(R.id.item_trip_date) as TextView

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view: View) {
            if(mClickListener != null){
                mClickListener.onItemClick(view,adapterPosition);
            }
        }

        override fun onLongClick(view: View): Boolean {
            if(mLongClickListener != null){
                mLongClickListener.onItemLongClick(view,adapterPosition);
            }
            return true
        }
    }

    // Create new views (invoked by the view manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): TripAdapter.ViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_trip, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and view parameters

        return ViewHolder(layout, mClickListener, mLongClickListener)
    }

    // Replace the contents of a view (invoked by the view manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val trip = myDataset[position]
        holder.trip_name.text = trip.name.capitalize()
        holder.trip_dates.text = "${calendarToText(trip.first_date)} - ${calendarToText(trip.last_date)}"
    }

    // Return the size of your dataset (invoked by the view manager)
    override fun getItemCount() = myDataset.size

    fun setTrips(trips: List<Trip>) {
        myDataset = trips
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(mClickListener: MyItemClickListener?){
        this.mClickListener = mClickListener
    }

    fun setOnItemLongClickListener(mLongClickListener: MyItemLongClickListener?){
        this.mLongClickListener = mLongClickListener
    }
}