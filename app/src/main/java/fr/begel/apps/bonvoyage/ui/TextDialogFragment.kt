package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import fr.begel.apps.bonvoyage.R
import java.util.*
import com.squareup.timessquare.CalendarPickerView
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_range_date_dialog.*
import kotlin.collections.ArrayList


class TextDialogFragment : DialogFragment() {

    private var textView: TextView? = null
    var mListener: TextDialogListener? = null

    interface TextDialogListener {
        fun onTextDialogPositiveClick(dialog: DialogFragment, name: String);
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as TextDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity.toString() + " must implement TextDialogListener")
        }
    }
    
    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        //Try here
        val view = inflater!!.inflate(R.layout.fragment_text_dialog, null)
        textView = view.findViewById(R.id.text_dialog_edit)
        val builder = AlertDialog.Builder(activity)
        // Use the Builder class for convenient dialog construction
        builder.setTitle(R.string.new_trip)
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                    mListener!!.onTextDialogPositiveClick(this, textView!!.text.toString())
                })
                .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                })
                .setView(view)
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }
}
