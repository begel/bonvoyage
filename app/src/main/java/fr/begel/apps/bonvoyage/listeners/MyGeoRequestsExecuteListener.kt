package fr.begel.apps.bonvoyage.listeners

import com.here.android.mpa.common.GeoCoordinate

class MyGeoRequestsExecuteListener {
    private var achieved = 0
    private var mListener: MListener? = null
    private var mList: MutableList<Pair<Boolean, GeoCoordinate>> = mutableListOf()
    private var expected: Int? = null

    interface MListener {
        fun onGeoRequestsEnd(list: MutableList<Pair<Boolean, GeoCoordinate>>)
    }

    fun onRequestEnd(element: Pair<Boolean, GeoCoordinate>){
        achieved += 1
        mList.add(element)
        if ((mListener != null) and (achieved == expected)){
            mListener!!.onGeoRequestsEnd(mList)
        }
    }

    fun setListener(listener: MListener){
        mListener = listener
    }

    fun setExpected(n: Int){
        expected = n
    }
}