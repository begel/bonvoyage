package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import java.util.*
import kotlin.collections.ArrayList

@Entity(tableName = "trip_days",
        foreignKeys = arrayOf(ForeignKey(
                entity = Trip::class,
                parentColumns = ["tripid"],
                childColumns = ["tripid"], onDelete = CASCADE)
        ),
        indices = arrayOf(Index("tripid"), Index("day_number")))
@TypeConverters(Converters::class)
data class TripDay (@ColumnInfo(name = "tripid") val tripId: String,
                    @ColumnInfo(name = "date") val date: Calendar,
                    @ColumnInfo(name = "day_number") val number: Long,
                    @PrimaryKey @ColumnInfo(name = "day_id") val dayId: String = UUID.randomUUID().toString(),
                    @ColumnInfo(name = "day_description") val name: String? = null
)

class TripWithDays {
    @Embedded val trip: Trip
    @Relation(parentColumn = "tripid", entityColumn = "tripid") var days: List<TripDay>

    constructor(trip: Trip){
        this.trip = trip
        this.days = emptyList()
    }
    fun sortedDays() = days.sortedBy { day -> day.date.timeInMillis }
}