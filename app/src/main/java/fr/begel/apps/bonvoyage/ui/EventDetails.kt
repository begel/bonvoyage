package fr.begel.apps.bonvoyage.ui

import android.os.Bundle
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import android.view.Menu
import android.view.MenuItem
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.R.id.event_details_edit
import fr.begel.apps.bonvoyage.R.id.event_details_delete
import fr.begel.apps.bonvoyage.persistence.EventWithExpenses

import kotlinx.android.synthetic.main.activity_event_details.*
import kotlinx.android.synthetic.main.app_bar_layout.*
import java.util.*
import kotlin.collections.ArrayList

const val EVENT_DETAILS_ID = "fr.begel.apps.bonvoyage.ui.event_details_id"

class EventDetails : AppCompatActivity() {

    private var eventWithExpenses : EventWithExpenses? = null
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventViewModel
    private lateinit var tripStartDay: Calendar
    private lateinit var tripEndDay: Calendar
    private lateinit var expenseAdapter: ExpenseAdapterSmall
    private lateinit var expenseManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)
        setSupportActionBar(toolbar)
        val eventId = intent.getStringExtra(EVENT_DETAILS_ID)
        tripStartDay = intent.getSerializableExtra(TRIP_START_DATE) as Calendar
        tripEndDay = intent.getSerializableExtra(TRIP_END_DATE) as Calendar
        viewModelFactory = Injection.provideViewModelFactory(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)
        viewModel.getEventByIdWithExpenses(eventId).observe (
                { -> this.lifecycle},
                {eventWithExpenses1: EventWithExpenses? ->
                    eventWithExpenses = eventWithExpenses1
                    if (eventWithExpenses1 != null) {
                        displayEvent()
                        expenseAdapter.setExpenses(eventWithExpenses1.expenses)
                    }}
        )
        eventWithExpenses = viewModel.getEventByIdWithExpenses(eventId).value
        //List of expenses
        expenseManager = LinearLayoutManager(this)
        expenseAdapter = ExpenseAdapterSmall(eventWithExpenses?.expenses ?: ArrayList())
        event_details_expense_list.layoutManager = expenseManager
        event_details_expense_list.adapter = expenseAdapter
    }

    private fun displayEvent(){
        if (eventWithExpenses != null) {
            event_details_icon.setImageDrawable(applicationContext
                    .getDrawable(eventWithExpenses!!.event.eventSubType.drawableId))
            event_details_name.text = eventWithExpenses!!.event.name
            event_details_address.text = orEmpty(eventWithExpenses!!.event.address)
            event_details_address_end.text = orEmpty(eventWithExpenses!!.event.addressEnd)
            event_details_booking.text = orEmpty(eventWithExpenses!!.event.bookingId)
            event_details_start.text = startSentence()
            event_details_end.text = endSentence()
            event_details_notes.text = orEmpty(eventWithExpenses!!.event.note)
            event_details_phone.text = orEmpty(eventWithExpenses!!.event.phone)
            event_details_ref.text = orEmpty(eventWithExpenses!!.event.reference)
            event_details_seat.text = orEmpty(eventWithExpenses!!.event.seat)
            event_details_subtype.text = applicationContext.getString(eventWithExpenses!!.event.eventSubType.stringId)
            hideUnusedRows()
        }
    }

    private fun startSentence(): String {
        var res = calendarToText(eventWithExpenses!!.event.dateStart)
        if (eventWithExpenses!!.event.timeStart != null) {
            res = res + " à " + calendarToHour(eventWithExpenses!!.event.timeStart)
        }
        if (eventWithExpenses!!.event.timeStartPlanned != null) {
            res = res + "\nY arriver à " + calendarToHour(eventWithExpenses!!.event.timeStartPlanned)
        }
        return res
    }

    private fun endSentence(): String {
        var res = calendarToText(eventWithExpenses!!.event.dateEnd)
        if (eventWithExpenses!!.event.timeEnd != null) {
            res = res + " à " + calendarToHour(eventWithExpenses!!.event.timeEnd)
        }
        if (eventWithExpenses!!.event.timeStartPlanned != null) {
            res = res + "\nRepartir à " + calendarToHour(eventWithExpenses!!.event.timeEndPlanned)
        }
        return res
    }

    private fun columnToView(): Map<TextView, Pair<Any?, TextView>> {
        return mapOf(
                event_details_address to (eventWithExpenses!!.event.address to event_details_address_label) ,
                event_details_address_end to (eventWithExpenses!!.event.addressEnd to event_details_address_end_label),
                event_details_booking to (eventWithExpenses!!.event.bookingId to event_details_booking_label),
                event_details_notes to (eventWithExpenses!!.event.note to event_details_notes_label),
                event_details_ref to (eventWithExpenses!!.event.reference to event_details_ref_label),
                event_details_phone to (eventWithExpenses!!.event.phone to event_details_phone_label),
                event_details_seat to (eventWithExpenses!!.event.bookingId to event_details_seat_label)
        )
    }

    private fun hideUnusedRows(){
        val dic = columnToView()
        for (v in dic.keys){
            if (dic[v]!!.first == null) {
                v.visibility = View.GONE
                dic[v]!!.second.visibility = View.GONE
            }
            else {
                v.visibility = View.VISIBLE
                dic[v]!!.second.visibility = View.VISIBLE
            }
        }
        if (eventWithExpenses!!.expenses.isEmpty()) {
            event_details_expense_label.visibility = View.GONE
            event_details_expense_list.visibility = View.GONE
        } else {
            event_details_expense_label.visibility = View.VISIBLE
            event_details_expense_list.visibility = View.VISIBLE

        }

    }
    //////////////////
    // MENU
    //////////////////
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_event_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item_event clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()
        when (id) {
            event_details_edit -> editEvent()
            event_details_delete -> deleteEvent()
        }
        return if ((id == event_details_edit) or (id == event_details_delete) ){
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun deleteEvent(){
        viewModel.deleteEventById(eventWithExpenses!!.event.id)
        finish()
    }

    private fun editEvent() {
        val intent = Intent(this, EventEdit::class.java).apply {
            putExtra(TRIP_ID, eventWithExpenses!!.event.tripId)
            putExtra(EVENT_ID, eventWithExpenses!!.event.id)
            putExtra(TRIP_START_DATE, tripStartDay)
            putExtra(TRIP_END_DATE, tripEndDay)
        }
        finish()
        startActivity(intent)
    }
}
