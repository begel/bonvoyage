package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import com.here.android.mpa.common.GeoCoordinate
import java.util.*

/*
Entity for events
 */

@Entity(tableName = "events",
        foreignKeys = [
            ForeignKey(entity = Trip::class, parentColumns = ["tripid"],
                    childColumns = ["trip_id"], onDelete = CASCADE)],
        indices = [Index("id"), Index("trip_id")])
@TypeConverters(Converters::class)
data class Event(@ColumnInfo(name = "name") val name: String,
                 @ColumnInfo(name = "trip_id") val tripId: String,
                 @ColumnInfo(name = "event_type") val eventType: EventType,
                 @ColumnInfo(name = "event_sub_type") val eventSubType: EventSubType,
                 @ColumnInfo(name = "date_start") val dateStart: Calendar,
                 @ColumnInfo(name = "date_end") val dateEnd: Calendar,
                 @ColumnInfo(name = "time_start") val timeStart: Calendar?,
                 @ColumnInfo(name = "time_start_planned") val timeStartPlanned: Calendar?,
                 @ColumnInfo(name = "time_end") val timeEnd: Calendar?,
                 @ColumnInfo(name = "time_end_planned") val timeEndPlanned: Calendar?,
                 @ColumnInfo(name = "address") val address: String?,
                 @ColumnInfo(name="coordinate") val coordinate: GeoCoordinate?,
                 @ColumnInfo(name = "address_end") val addressEnd: String?,
                 @ColumnInfo(name="coordinate_end") val coordinate_end: GeoCoordinate?,
                 @ColumnInfo(name = "note") val note: String?,
                 @ColumnInfo(name = "reference") val reference: String?,
                 @ColumnInfo(name = "phone") val phone : String?,
                 @ColumnInfo(name = "seat") val seat : String?,
                 @ColumnInfo(name = "booking_id") val bookingId: String?,
                 @ColumnInfo(name = "last_modified") val lastModified : Calendar = GregorianCalendar.getInstance(),
                 @PrimaryKey @ColumnInfo(name = "id") val id: String = UUID.randomUUID().toString()
)