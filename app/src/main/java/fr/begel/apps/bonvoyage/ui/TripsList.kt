package fr.begel.apps.bonvoyage.ui

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DialogFragment
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.provider.DocumentFile
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.MyItemClickListener
import fr.begel.apps.bonvoyage.listeners.MyItemLongClickListener
import fr.begel.apps.bonvoyage.persistence.*

import kotlinx.android.synthetic.main.activity_trips_list.*
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStreamReader
import java.util.*


class TripsList : AppCompatActivity(),
        RangeDateDialogFragment.RangeDateDialogListener, TextDialogFragment.TextDialogListener,
        MyItemClickListener, MyItemLongClickListener {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: TripAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: TripViewModel
    private var newTripName: String? = null

    private var trips: List<Trip>? = null
    private var events: List<Event>? = null
    private var expenses: List<Expense>? = null
    private var tripDays: List<TripDay>? = null
    private var eventsAndDays: List<EventsAndDays>? = null
    private val requestCodeExport = 1
    private val requestCodeImport = 2

    // permissions request code
    private val REQUEST_CODE_ASK_PERMISSIONS = 199

    /**
     * Permissions that need to be explicitly requested from end user.
     */
    private val REQUIRED_SDK_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trips_list)
        setSupportActionBar(toolbar)

        fab.setOnClickListener{_ -> displayTextDialog()}
        viewManager = LinearLayoutManager(this)
        viewAdapter = TripAdapter(mutableListOf<Trip>())
        viewAdapter.setOnItemClickListener(this)
        viewAdapter.setOnItemLongClickListener(this)
        recyclerView = findViewById<RecyclerView>(R.id.recycler_view).apply{
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        viewModelFactory = Injection.provideViewModelFactory(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TripViewModel::class.java)

        viewModel.getAllTrips().observe (
                { -> this.lifecycle},
                {trips: List<Trip>? ->
                    viewAdapter.setTrips(trips!!)
                    this.trips = trips
                    ifReadyExport()
                }
        )
        MyData(applicationContext).populate(false)
        checkPermissions()
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, dates : MutableList<Date>) {
        val date_start = dates.first()
        val date_end = dates.last()
        val trip = Trip(newTripName!!, dateToCalendar(date_start), dateToCalendar(date_end))
        viewModel.insertTrip(trip)
    }

    override fun onTextDialogPositiveClick(dialog: DialogFragment, name: String) {
        newTripName = name
        displayDateDialogFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_trips_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item_trip clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_backup -> clickExport()
            R.id.action_restore -> clickImport()
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun displayDateDialogFragment(){
        val dialogFragment = RangeDateDialogFragment()
        val args = Bundle()
        val later = GregorianCalendar()
        later.add(GregorianCalendar.YEAR, 5)
        args.putSerializable(ARG_DATE_START, Date())
        args.putSerializable(ARG_DATE_END, later.time)
        dialogFragment.arguments = args
        dialogFragment.show(fragmentManager, "new_trip_date")
    }

    private fun displayTextDialog(){
        val dialogFragment = TextDialogFragment()
        dialogFragment.show(fragmentManager, "new_trip_name")
    }

    override fun onItemClick(view: View, position: Int){
        val tripid = viewModel.getAllTrips().value!!.get(position).id
        /*val intent = Intent(this, EventsOfTrip::class.java).apply {
            putExtra(SUMMARY_TRIP_ID, tripid)
        }*/
        val intent = Intent(this, TripMain::class.java).apply {
            putExtra(SUMMARY_TRIP_ID, tripid)
        }
        startActivity(intent)
    }

    override fun onItemLongClick(view: View, position: Int){
        val builder = AlertDialog.Builder(this)
        val tripId = viewModel.getAllTrips().value!!.get(position).id
        builder.setMessage(R.string.delete_trip)
                .setPositiveButton(R.string.delete){
                    _: DialogInterface, _: Int ->
                    viewModel.deleteTrip(tripId)
                }
                .setNegativeButton(R.string.cancel){
                    dialog: DialogInterface, id: Int ->
                    dialog.cancel()
                }
        val dialog = builder.create()
        dialog.show()
    }

    // EXPORT AS JSON
    private fun clickExport(): Boolean{
        viewModel.getAllEvents().observe({this.lifecycle})
        {list: List<Event>? ->
            events = list
            ifReadyExport()
        }
        viewModel.getAllExpenses().observe({this.lifecycle})
        {list: List<Expense>? ->
            expenses = list
            ifReadyExport()
        }
        viewModel.getAllTripDays().observe({this.lifecycle})
        {list: List<TripDay>? ->
            tripDays = list
            ifReadyExport()
        }
        viewModel.getAllEventsAndDays().observe({this.lifecycle})
        {list: List<EventsAndDays>? ->
            eventsAndDays = list
            ifReadyExport()
        }
        return true
    }

    private fun ifReadyExport(){
        if ((trips != null) and (events != null) and (expenses != null)
                and (tripDays != null) and (eventsAndDays != null)){
            Log.d("Help", "Ready to export")
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            startActivityForResult(intent, requestCodeExport)
        } else{
            Log.d("Help", "Not Ready to export")
        }
    }

    private fun export(uri: Uri){
        val allElements = AllElements(trips!!, events!!, expenses!!, tripDays!!, eventsAndDays!!)
        val output = Gson().toJson(allElements)
        val pickedDir = DocumentFile.fromTreeUri(this, uri)
        val newFile = pickedDir.createFile(
                "application/json",
                "bonvoyage.json")
        val out = contentResolver.openOutputStream(newFile.uri)
        out.write(output.toByteArray())
        out.close()
        Log.d("Help", "JSON written at ${newFile.uri}")
    }

    // IMPORT JSON FILE
    private fun clickImport(): Boolean{
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        startActivityForResult(intent, requestCodeImport)
        return true
    }

    private fun import(uri: Uri){
        val fd = contentResolver.openFileDescriptor(uri, "r")
        val src = FileInputStream(fd.fileDescriptor)
        val reader = BufferedReader(InputStreamReader(src))
        val sb = StringBuilder()
        var line: String? = reader.readLine()
        while (line != null) {
            sb.append(line)
            line = reader.readLine()
        }
        reader.close()
        src.close()
        val json = sb.toString()
        val allElements = Gson().fromJson(json, AllElements::class.java)
        viewModel.insertAllElements(allElements)
        Log.d("Help", "Import succeed")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode){
                requestCodeExport -> export(data!!.data)
                requestCodeImport -> import(data!!.data)
                else -> return
            }
        }
    }

    class AllElements(
            val trips: List<Trip>,
            val events: List<Event>,
            val expenses: List<Expense>,
            val tripDays: List<TripDay>,
            val eventsAndDays: List<EventsAndDays>
    )

    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected fun checkPermissions() {
        val missingPermissions = ArrayList<String>()
        // check all required dynamic permissions
        for (permission in REQUIRED_SDK_PERMISSIONS) {
            val result = ContextCompat.checkSelfPermission(this, permission)
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission)
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            val permissions = missingPermissions.toTypedArray()
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS)
        } else {
            val grantResults = IntArray(REQUIRED_SDK_PERMISSIONS.size)
            grantResults.fill(PackageManager.PERMISSION_GRANTED)
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_ASK_PERMISSIONS -> {
                for (index in permissions.indices) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show()
                        finish()
                        return
                    }
                }
            }
        }
    }
}
