package fr.begel.apps.bonvoyage.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import fr.begel.apps.bonvoyage.Injection

import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.persistence.Expense
import fr.begel.apps.bonvoyage.persistence.ExpenseWithName
import fr.begel.apps.bonvoyage.persistence.TotalExpense
import fr.begel.apps.bonvoyage.persistence.Trip
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ExpensesOfTrip.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ExpensesOfTrip.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ExpensesOfTrip : Fragment() {
    // TODO: Rename and change types of parameters
    private var totals: List<TotalExpense> = emptyList()
    private var expenses: List<ExpenseWithName> = ArrayList()
    private lateinit var footer: ConstraintLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventViewModel
    private lateinit var viewAdapter: ExpenseAdapter
    private var tripId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            tripId = it.getString(TRIP_ID)
        }
        viewModelFactory = Injection.provideViewModelFactory(context!!)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)
        viewModel.totalExpensesOfTrip(tripId!!).observe (
                { -> this.lifecycle},
                {totals: List<TotalExpense>? ->
                    this.totals = totals ?: emptyList()
                    updateTotal()}
        )
        viewModel.getExpensesOfTripName(tripId!!).observe (
                { -> this.lifecycle},
                {events: List<ExpenseWithName>? ->
                    viewAdapter.setExpenses(events!!)
                    this.expenses = events
                }
        )
        viewAdapter = ExpenseAdapter(expenses)
    }

    private fun updateTotal(){
        var res = ""
        for (total in totals){
            if (res.isNotEmpty()) res += "\n"
            res += total.total.toString() + Currency.getInstance(total.currency).symbol
        }
        (footer.getViewById(R.id.item_expense_amount) as TextView).text = res
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_expenses_of_trip, container, false)
        footer = view.findViewById(R.id.expense_footer)
        (footer.getViewById(R.id.item_expense_date) as TextView).text = ""
        (footer.getViewById(R.id.item_expense_title) as TextView).text = "TOTAL"
        updateTotal()
        recyclerView = view.findViewById<RecyclerView>(R.id.expenses_recycler_view).apply{
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = viewAdapter
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(tripId: String) =
                ExpensesOfTrip().apply {
                    arguments = Bundle().apply {
                        putString(TRIP_ID, tripId)
                    }
                }
    }
}
