package fr.begel.apps.bonvoyage.ui

import android.arch.lifecycle.ViewModel
import android.content.Context
import fr.begel.apps.bonvoyage.persistence.*


class EventViewModel(val context: Context) : ViewModel() {

    private val mRepository = AppRepository(context)

    fun insertEvent(event: Event, days: List<TripDay>) {
        mRepository.insertEvent(event, days)
    }

    fun getTripById(id: String) = mRepository.getTripById(id)

    fun getTripDays(tripId: String) = mRepository.getTripDaysOfTrip(tripId)

    fun getTripDaysWithEventsId(tripId: String) = mRepository.getTripDaysWithEventsId(tripId)

    fun getEventsOfTrip(id: String) = mRepository.getEventsOfTrip(id)

    fun deleteEventById(id: String) = mRepository.deleteEventById(id)

    fun insertExpense(expense: Expense) = mRepository.insertExpense(expense)

    fun getEventByIdWithExpenses(eventId: String) = mRepository.getEventByIdWithExpenses(eventId)

    fun getExpensesOfTripName(tripId: String) = mRepository.getExpensesOfTripName(tripId)

    fun totalExpensesOfTrip(tripId: String) = mRepository.totalExpensesOfTrip(tripId)

    fun getTripWithDays(tripId: String) = mRepository.getTripWithDays(tripId)

    fun getRoutesByTripId(tripId: String) = mRepository.getRoutesByTripId(tripId)

    fun insertRoute(element: RouteBtwEvents) = mRepository.insertRoute(element)

    fun insertAllRoutes(elements: List<RouteBtwEvents>) = mRepository.insertAllRoutes(elements)
}
