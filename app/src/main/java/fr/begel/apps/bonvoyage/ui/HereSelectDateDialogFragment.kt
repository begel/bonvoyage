package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.persistence.TripDay
import fr.begel.apps.bonvoyage.persistence.TripDayWithEvents

const val HERE_SELECT_CHOICES = "fr.begel.apps.bonvoyage.ui.map_select_dates"
const val HERE_SELECTED = "fr.begel.apps.bonvoyage.ui.map_selected_dates"

class HereSelectDateDialogFragment: DialogFragment() {

    private lateinit var mSelectedItems: ArrayList<TripDayWithEvents>
    var mListener: HereSelectDateDialogListener? = null
    private lateinit var choices: ArrayList<TripDayWithEvents>

    // Interface to link the implemented function to the mListener
    interface HereSelectDateDialogListener {
        fun onSelectDateDialogPositiveClick(dialog: DialogFragment, tripDays : ArrayList<TripDayWithEvents>);
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as HereSelectDateDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity.toString() + " must implement HereSelectDateDialogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Argument
        arguments?.let {
            choices = it.getSerializable(HERE_SELECT_CHOICES) as ArrayList<TripDayWithEvents>
            mSelectedItems = it.getSerializable(HERE_SELECTED) as ArrayList<TripDayWithEvents>
        }
        val charSequences = Array(choices.size) {i ->
            val day = choices[i].tripDay
            var str = dateToShortText(day.date.time)
            if (!day.name.isNullOrEmpty()){
                str += " - " + day.name
            }
            str
        }
        val selectedChoices = BooleanArray(choices.size) {
            i -> mSelectedItems.contains(choices[i])
        }
        val builder = AlertDialog.Builder(activity);
        // Build the Dialog
        builder.setTitle(R.string.select_date)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setMultiChoiceItems(charSequences, selectedChoices)
                {_, which, isChecked ->
                    val tripDay = choices[which]
                    if (isChecked) {
                        mSelectedItems.add(tripDay);
                    } else if (mSelectedItems.contains(tripDay)) {
                        mSelectedItems.remove(tripDay)
                    }
                }
                // Set the action buttons
                .setPositiveButton(R.string.ok) { dialog, id ->
                    mListener!!.onSelectDateDialogPositiveClick(this, mSelectedItems)
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                }
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(choices: ArrayList<TripDayWithEvents>, selected: ArrayList<TripDayWithEvents>) =
                HereSelectDateDialogFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(HERE_SELECT_CHOICES, choices)
                        putSerializable(HERE_SELECTED, selected)
                    }
                }
    }
}