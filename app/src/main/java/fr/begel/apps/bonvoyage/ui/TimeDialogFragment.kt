package fr.begel.apps.bonvoyage.ui

import android.app.Dialog
import android.app.DialogFragment
import android.widget.TimePicker
import android.text.format.DateFormat.is24HourFormat
import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.TextView

const val TIME_TEXTVIEW_ID = "fr.begel.apps.bonvoyage.ui.timedialogfragment.viewid"
const val TIME_DEFAULT_HOUR = "fr.begel.apps.bonvoyage.ui.timedialogfragment.hour"
const val TIME_DEFAULT_MIN = "fr.begel.apps.bonvoyage.ui.timedialogfragment.minute"

class TimeDialogFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private lateinit var textView: TextView
    private var hourOfDay: Int? = null
    private var minute: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        arguments?.let {
            val textViewId = it.getSerializable(TIME_TEXTVIEW_ID) as Int
            textView = activity.findViewById(textViewId)
            hourOfDay = it.getSerializable(TIME_DEFAULT_HOUR) as Int? ?: 12
            minute = it.getSerializable(TIME_DEFAULT_MIN) as Int? ?:0
        }
        // Create a new instance of TimePickerDialog and return it
        return TimePickerDialog(activity, this, hourOfDay!!, minute!!,
                is24HourFormat(activity))
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        val min: String = when (minute < 10){
            true -> "0$minute"
            false -> "$minute"
        }
        textView.setText("${hourOfDay}:$min")
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param viewId Parameter 1.
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(textViewId: Int, hourOfDay: Int?, minute: Int?) =
                TimeDialogFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(TIME_TEXTVIEW_ID, textViewId)
                        putSerializable(TIME_DEFAULT_HOUR, hourOfDay)
                        putSerializable(TIME_DEFAULT_MIN, minute)
                    }
                }
    }
}
