package fr.begel.apps.bonvoyage.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import fr.begel.apps.bonvoyage.persistence.TripDAO

/**
 * Factory for ViewModels
 */
class ViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TripViewModel::class.java)) {
            return TripViewModel(context) as T
        }
        if (modelClass.isAssignableFrom(EventViewModel::class.java)) {
            return EventViewModel(context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}