package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import java.util.*

/*
Entity for trips
 */

@Entity(tableName = "trips", indices = [(Index("tripid"))])
@TypeConverters(Converters::class)
data class Trip(@ColumnInfo(name = "name")
                val name: String,
                @ColumnInfo(name = "first_date")
                val first_date: Calendar,
                @ColumnInfo(name = "last_date")
                val last_date: Calendar,
                @PrimaryKey
                @ColumnInfo(name = "tripid")
                val id: String = UUID.randomUUID().toString(),
                @ColumnInfo(name = "photo")
                val photo: String? = null
                )