package fr.begel.apps.bonvoyage.listeners

import android.view.View

interface MyItemLongClickListener {
    fun onItemLongClick(view: View, position: Int)
}