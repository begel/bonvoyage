package fr.begel.apps.bonvoyage.listeners

import android.view.View

interface MyItemClickListener {
    fun onItemClick(view: View, position: Int)
}