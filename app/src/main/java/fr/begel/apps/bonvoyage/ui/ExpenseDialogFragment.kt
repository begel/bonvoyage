package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.app.FragmentManager
import android.content.Context
import android.os.Bundle
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import fr.begel.apps.bonvoyage.R
import java.util.*
import com.squareup.timessquare.CalendarPickerView
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_range_date_dialog.*
import kotlin.collections.ArrayList

class ExpenseDialogFragment() : DialogFragment() {

    private lateinit var editText: EditText
    private lateinit var spinner: Spinner
    private lateinit var mView: View
    var mListener: ExpenseDialogListener? = null

    // Interface to link the implemented function to the mListener
    interface ExpenseDialogListener {
        fun onExpenseDialogPositiveClick(dialog: DialogFragment, amount: Float, currency: String);
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as ExpenseDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity.toString() + " must implement NewExpenseDialogListener")
        }
    }
    
    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    // OnCreate
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        //Component
        mView = inflater!!.inflate(R.layout.fragment_expense_dialog, null)
        editText = mView.findViewById(R.id.expense_dialog_amount)
        spinner = mView.findViewById(R.id.expense_dialog_currency)
        // Spinner - fill with currency' symbols
        val dataAdapterType = ArrayAdapter(mView.context,
                android.R.layout.simple_spinner_item,
                currencies.map { element -> element.second })
        dataAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.setAdapter(dataAdapterType)
        //Builder
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.amount)
                .setPositiveButton(R.string.next, null)
                .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                })
                .setView(mView)
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }

    override fun show(manager: FragmentManager?, tag: String?) {
        super.show(manager, tag)

    }

    override fun onResume() {
        super.onResume()
        val alertDialog = dialog as AlertDialog
        val button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        button.setOnClickListener(View.OnClickListener { view ->
            // choice is the ID of the selected Item
            val currency = currencies[spinner.selectedItemPosition].first
            val amount = editText.text.toString()
            if (!amount.isEmpty()) {
                mListener!!.onExpenseDialogPositiveClick(this, amount.toFloat(), currency)
                dismiss()
            } else
                Toast.makeText(view.context, "Montant obligatoire", Toast.LENGTH_LONG).show()
        })
    }
}
