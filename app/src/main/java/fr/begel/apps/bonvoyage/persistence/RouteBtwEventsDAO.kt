package fr.begel.apps.bonvoyage.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

/**
 * Data Access Object for the trip table.
 */
@Dao
interface RouteBtwEventsDAO {

    @Query("SELECT * FROM routes WHERE trip_id = :tripId")
    fun getRoutesByTripId(tripId: String): LiveData<List<RouteBtwEvents>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoute(element: RouteBtwEvents)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllRoutes(elements: List<RouteBtwEvents>)
}