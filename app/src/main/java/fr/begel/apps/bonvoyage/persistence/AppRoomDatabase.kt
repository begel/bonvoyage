package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context


const val DATABASE_NAME = "bonvoyage.db"
/**
 * The Room database that contains the Users table
 */
@Database(entities = arrayOf(
        Trip::class, Event::class, Expense::class,
        TripDay::class, EventsAndDays::class, RouteBtwEvents::class),
        version = 2)
abstract class AppRoomDatabase : RoomDatabase() {

    abstract fun tripDao(): TripDAO
    abstract fun eventDao(): EventDAO
    abstract fun expenseDao(): ExpenseDAO
    abstract fun tripDayDao(): TripDayDAO
    abstract fun eventsAndDaysDao(): EventsAndDaysDAO
    abstract fun routeBtwEventsDao(): RouteBtwEventsDAO

    companion object {

        private var INSTANCE: AppRoomDatabase? = null


        internal fun getDatabase(context: Context): AppRoomDatabase {
            if (INSTANCE == null) {
                synchronized(AppRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                //Room.inMemoryDatabaseBuilder(context.applicationContext,
                                AppRoomDatabase::class.java
                                , DATABASE_NAME
                                )
                                .build()

                    }
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance(){
            if (INSTANCE?.isOpen == true)
                INSTANCE!!.close()
            INSTANCE = null
        }
    }

}