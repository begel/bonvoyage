package fr.begel.apps.bonvoyage.ui

import android.content.Context
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*
import android.widget.EditText
import kotlin.collections.ArrayList
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Environment
import android.util.Log
import android.widget.Toast
import fr.begel.apps.bonvoyage.persistence.AppRoomDatabase
import fr.begel.apps.bonvoyage.persistence.DATABASE_NAME
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.locks.Lock
import kotlin.concurrent.withLock


val local: Locale = Locale.FRANCE
const val textFormat = "EEE d MMM yy" // mention the format you need
const val shortTextFormat = "EEE d"
const val strFormat = "yyyy-MM-dd"
const val strInvFormat = "dd/MM/yyyy"
const val hour = "k:mm"

fun dateToCalendar(date: Date): Calendar {
    val res = Calendar.getInstance()
    res.timeInMillis = date.time
    return res
}

fun calendarToDate(cal: Calendar): Date {
    return cal.time
}

fun dateToStr(date: Date, format: String): String {
    val sdf = SimpleDateFormat(format, local)
    return sdf.format(date)
}

fun dateToText(date: Date): String {
    return dateToStr(date, textFormat)
}

fun dateToStr(date: Date): String {
    return dateToStr(date, strFormat)
}

fun dateToShortText(date: Date): String {
    return dateToStr(date, shortTextFormat)
}

fun calendarToText(cal: Calendar): String {
    return dateToText(cal.time)
}

fun calendarToStr(cal: Calendar): String {
    return dateToStr(cal.time)
}

fun calToShortText(cal: Calendar): String {
    return dateToShortText(cal.time)
}

fun strToDate(str: String, format: String): Date {
    val sdf = SimpleDateFormat(format, local)
    return sdf.parse(str)
}

fun strToDate(str: String): Date {
    return strToDate(str, strFormat)
}

fun textToDate(str: String): Date {
    return strToDate(str, textFormat)
}

fun strToCalendar(str: String): Calendar {
    return dateToCalendar(strToDate(str))
}

fun str2ToCalendar(str: String) = dateToCalendar(strToDate(str, strInvFormat))

fun textToCalendar(str: String): Calendar {
    return dateToCalendar(textToDate(str))
}

fun dateToHour(date: Date) = dateToStr(date, hour)

fun calendarToHour(cal: Calendar?): String {
    if (cal == null) {
        return ""
    }
    return dateToHour(cal.time)
}

fun orEmpty(str: String?): String {
    if (str == null) return ""
    return str
}

fun limitsDates(dates: ArrayList<Date>): ArrayList<Date>{
    when (dates.size) {
        0 -> return dates
        1 -> return dates
        else -> {val tmp = ArrayList<Date>()
            tmp.add(dates.first())
            tmp.add(dates.last())
            return tmp}
    }
}

fun textToHour(hour: String?): Pair<Int, Int> {
    if (hour.isNullOrEmpty())
        return 12 to 0
    val sdf = SimpleDateFormat("H:m", local)
    val date = dateToCalendar(sdf.parse(hour))
    return date.get(Calendar.HOUR_OF_DAY) to date.get(Calendar.MINUTE)
}

fun nullIfEmpty(view: EditText): String? {
    if (view.text.toString().isEmpty())
        return null
    else
        return view.text.toString()
}

fun hourToCalendar(hour: String): Calendar {
    val sdf = SimpleDateFormat("H:m", local)
    return dateToCalendar(sdf.parse(hour))
}

fun viewHourToCalendar(view: EditText): Calendar? {
    val hour = view.text.toString()
    if (hour.isEmpty())
        return null
    val sdf = SimpleDateFormat("H:m", local)
    return dateToCalendar(sdf.parse(hour))
}

val currencies: List<Pair<String, String>> =
        ArrayList<String>().apply {
            add("USD")
            add("EUR")
        }.map { c -> c to Currency.getInstance(c).symbol }

fun getBitmapFromVectorDrawable(drawable: Drawable?): Bitmap {
    val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth,
            drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
    drawable.draw(canvas)

    return bitmap
}

fun diffCalendar(cal1: Calendar, cal2: Calendar): Long {
    val cal10 = GregorianCalendar.getInstance().apply {
        set(cal1.get(YEAR), cal1.get(MONTH), cal1.get(DAY_OF_MONTH), 0, 0, 0)
    }
    val cal20 = GregorianCalendar.getInstance().apply {
        set(cal2.get(YEAR), cal2.get(MONTH), cal2.get(DAY_OF_MONTH), 0, 0, 0)
    }
    return (cal10.timeInMillis - cal20.timeInMillis) / (24 * 60 * 60 * 1000)
}

fun hourToInt(cal: Calendar): Int {
    return cal.get(HOUR_OF_DAY) * 100 + cal.get(MINUTE)
}

// Backup & Restore BD
/* Checks if external storage is available for read and write */
private fun isExternalStorageWritable(): Boolean {
    return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
}

/* Checks if external storage is available to at least read */
private fun isExternalStorageReadable(): Boolean {
    return Environment.getExternalStorageState() in
            setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
}

// Copy DB
@Throws(IOException::class)
fun copyAppDbToDownloadFolder(context: Context): Boolean {
    if (isExternalStorageWritable()) {
        AppRoomDatabase.destroyInstance()
        val prefix = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val backupDB = File(prefix, DATABASE_NAME)
        val currentDB = context.getDatabasePath(DATABASE_NAME)
        if (currentDB.exists()) {
            val src = FileInputStream(currentDB).channel
            val dst = FileOutputStream(backupDB).channel
            Log.d("Help", "Source size Export: ${src.size()}")
            src.transferTo(0, src.size(), dst)
            //dst.transferFrom(src, 0, src.size())
            src.close()
            dst.close()
        }
        return true
    } else
        Toast.makeText(context, "Storage not writable, backup not possible", Toast.LENGTH_LONG).show()
    return false
}

// Copy DB
@Throws(IOException::class)
fun restoreAppDbFromDownloadFolder(context: Context): Boolean {
    if (isExternalStorageReadable()) {
        AppRoomDatabase.destroyInstance()
        val prefix = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val backupDB = File(prefix, DATABASE_NAME)
        val currentDB = context.getDatabasePath(DATABASE_NAME)
        if (backupDB.exists()) {
            val dst = FileOutputStream(currentDB).channel
            val src = FileInputStream(backupDB).channel
            Log.d("Help", "Source size Import: ${src.size()}")
            dst.transferFrom(src, 0, src.size())
            src.close()
            dst.close()
            Toast.makeText(context, "Database imported", Toast.LENGTH_LONG).show()
        }
        return true
    } else
        Toast.makeText(context, "Storage not readable, restore not possible", Toast.LENGTH_LONG).show()
    return false
}

//REF: https://jonnyzzz.com/blog/2017/03/01/guarded-by-lock/
class GuardedByLock<out L: Lock, out T>(
        val lock: L,
        val state: T
) {
    inline fun <Y> runWithLock(action: T.() -> Y) = lock.withLock { state.action() }
}

fun fromSecondToTime(seconds: Int?): String{
    if (seconds == null) return "Temps inconnu"
    if (seconds < 60) return "$seconds s"
    var minutes = seconds / 60
    if (minutes < 60) return "$minutes min"
    val hours = minutes / 60
    minutes = minutes.rem(60)
    return "$hours h $minutes min"
}