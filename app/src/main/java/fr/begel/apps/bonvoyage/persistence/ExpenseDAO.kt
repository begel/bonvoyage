package fr.begel.apps.bonvoyage.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

/**
 * Data Access Object for the expense table.
 */
@Dao
interface ExpenseDAO {

    @Query("SELECT coalesce(events.name, expenses.name, '') as display, " +
            "expenses.* from expenses " +
            "left join events on expenses.event_id = events.id " +
            "WHERE expenses.trip_id = :tripid " +
            "ORDER BY expenses.date")
    fun getExpensesOfTripName(tripid: String): LiveData<List<ExpenseWithName>>

    @Query("SELECT SUM(amount) as total, currency FROM expenses WHERE trip_id = :tripid GROUP BY currency")
    fun totalExpensesOfTrip(tripid: String): LiveData<List<TotalExpense>>

    /**
     * Insert a expense in the database. If the expense already exists, replace it.

     * @param expense the expense to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExpense(expense: Expense)

    /**
     * Delete one expense by id.
     * @param expenseid the expenseid to be deleted
     */
    @Query("DELETE FROM expenses where id = :id")
    fun deleteExpenseById(id: String)

    @Transaction
    @Query("SELECT * FROM events where id = :event_id")
    fun getEventByIdWithExpenses(event_id: String): LiveData<EventWithExpenses>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllExpenses(elements: List<Expense>)

    @Query("SELECT * FROM expenses")
    fun getAllExpenses(): LiveData<List<Expense>>
}