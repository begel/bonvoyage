package fr.begel.apps.bonvoyage.persistence

import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.persistence.EventSubType.*

enum class EventType(val stringId: Int, val id: Int) {
    HOSTING(R.string.hosting, 1){
        override fun subTypes(): ArrayList<EventSubType> {
            return hostingSubType()
        }
    },
    ACTIVITY(R.string.activity, 2){
        override fun subTypes(): ArrayList<EventSubType> {
            return activitySubType()
        }
    },
    TRANSPORTATION(R.string.transportation, 3){
        override fun subTypes(): ArrayList<EventSubType> {
            return transportSubType()
        }
    };

    abstract fun subTypes(): ArrayList<EventSubType>

    fun hostingSubType(): ArrayList<EventSubType>{
        val collection = ArrayList<EventSubType>()
        collection.add(EventSubType.AIRBNB)
        collection.add(HOTEL)
        return collection
    }

    fun activitySubType(): ArrayList<EventSubType>{
        val collection = ArrayList<EventSubType>()
        collection.add(HIKE)
        collection.add(SIGHTSEEING)
        collection.add(SELFVISIT)
        collection.add(TOUR)
        return collection
    }

    fun transportSubType(): ArrayList<EventSubType>{
        val collection = ArrayList<EventSubType>()
        collection.add(RENTACAR)
        collection.add(FLIGHT)
        collection.add(BUS)
        collection.add(TRAIN)
        collection.add(BOAT)
        return collection
    }
}