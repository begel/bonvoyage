package fr.begel.apps.bonvoyage.persistence

import android.os.AsyncTask
import android.content.Context
import android.util.Log
import fr.begel.apps.bonvoyage.ui.TripsList
import fr.begel.apps.bonvoyage.ui.calendarToStr
import java.util.*
import kotlin.collections.ArrayList


class AppRepository(val context : Context) {

    private val db = AppRoomDatabase.getDatabase(context)
    private val mTripDAO = db.tripDao()
    private val mEventDAO = db.eventDao()
    private val mEventsAndDaysDAO = db.eventsAndDaysDao()
    private val mExpenseDAO = db.expenseDao()
    private val mTripDayDAO = db.tripDayDao()
    private val mRouteBtwEventsDAO = db.routeBtwEventsDao()
    private val allTrips = mTripDAO.getAllTrips()

    // TRIPDAYS
    fun insertAllTripDays(tripDays: Collection<TripDay>) = insertAllTripDaysAsyncTask(mTripDayDAO).execute(tripDays)
    private class insertAllTripDaysAsyncTask (private val mAsyncTripDayDao: TripDayDAO) :
            AsyncTask<Collection<TripDay>, Void, Void>() {
        override fun doInBackground(vararg params: Collection<TripDay>): Void? {
            mAsyncTripDayDao.insertAllTripDays(params[0])
            return null
        }
    }

    fun getTripWithDays(tripid: String) = mTripDayDAO.getTripWithDays(tripid)
    fun getTripDaysOfTrip(tripid: String) = mTripDayDAO.getTripDays(tripid)
    fun getTripDaysWithEventsId(tripid: String) = mTripDayDAO.getTripDaysWithEventsId(tripid)
    fun getAllTripDays() = mTripDayDAO.getAllTripDays()

    //TRIPS
    fun getAllTrips() = allTrips

    fun insertTrip(trip: Trip): ArrayList<TripDay> {
        insertTripAsyncTask(mTripDAO).execute(trip)
        val date = trip.first_date.clone() as Calendar
        var nb_days = 0L
        val trip_days = arrayListOf<TripDay>()
        while (date.compareTo(trip.last_date) < 1){
            val td = TripDay(trip.id, date.clone() as Calendar, nb_days)
            trip_days.add(td)
            date.add(Calendar.DATE, 1)
            nb_days += 1
        }
        insertAllTripDays(trip_days)
        return trip_days
    }

    fun deleteAllTrips() = deleteAllTripsAsyncTask(mTripDAO).execute()

    fun deleteTripById(tripId: String) = deleteTripByIdAsyncTask(mTripDAO, tripId).execute()

    fun getTripById(id: String) = mTripDAO.getTripById(id)

    private class insertTripAsyncTask (private val mAsyncTaskDao: TripDAO) : AsyncTask<Trip, Void, Void>() {
        override fun doInBackground(vararg params: Trip): Void? {
            mAsyncTaskDao.insertTrip(params[0])
            return null
        }
    }

    private class deleteAllTripsAsyncTask (private val mAsyncTripDao: TripDAO) : AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg params: String): Void? {
            mAsyncTripDao.deleteAllTrips()
            return null
        }
    }

    private class deleteTripByIdAsyncTask (private val mAsyncTripDao: TripDAO, private val tripId: String) :
            AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg params: String): Void? {
            mAsyncTripDao.deleteTripById(tripId)
            return null
        }
    }

    //EVENTS AND DAYS
    fun getAllEventsAndDays() = mEventsAndDaysDAO.getAllEventsAndDays()
    fun insertEventsAndDays(element: EventsAndDays) {
        insertEventsAndDaysAsyncTask(mEventsAndDaysDAO).execute(element)
    }
    fun deleteEventsAndDaysOfEvent(eventId: String) {
        deleteEventsAndDaysAsyncTask(mEventsAndDaysDAO).execute(eventId)
    }
    private class insertEventsAndDaysAsyncTask (private val mAsyncEventsAndDaysDao: EventsAndDaysDAO) :
            AsyncTask<EventsAndDays, Void, Void>() {
        override fun doInBackground(vararg params: EventsAndDays): Void? {
            mAsyncEventsAndDaysDao.insertEventsAndDays(params[0])
            return null
        }
    }
    private class deleteEventsAndDaysAsyncTask (private val mAsyncEventsAndDaysDao: EventsAndDaysDAO) :
            AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg params: String): Void? {
            mAsyncEventsAndDaysDao.deleteEventsAndDaysOfEvent(params[0])
            return null
        }
    }


    //EVENTS
    fun getEventsOfTrip(id: String) = mEventDAO.getEventsOfTrip(id)
    fun getAllEvents() = mEventDAO.getAllEvents()

    fun insertEventOnly(event: Event) {
        insertEventAsyncTask(mEventDAO).execute(event)
    }
    fun deleteEventById(id: String) {
        deleteEventAsyncTask(mEventDAO).execute(id)
    }

    private class insertEventAsyncTask (private val mAsyncEventDao: EventDAO) : AsyncTask<Event, Void, Void>() {
        override fun doInBackground(vararg params: Event): Void? {
            mAsyncEventDao.insertEvent(params[0])
            return null
        }
    }

    private class deleteEventAsyncTask (private val mAsyncEventDao: EventDAO) : AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg params: String): Void? {
            mAsyncEventDao.deleteEventById(params[0])
            return null
        }
    }

    fun insertEvent(event: Event, days: List<TripDay>){
        insertEventOnly(event)
        deleteEventsAndDaysOfEvent(event.id)
        Log.d("Populate", "Insert Event ${event.name}")
        if (event.eventType == EventType.HOSTING){
            for (day in days){
                if ((event.dateStart.compareTo(day.date) <= 0) and (day.date.compareTo(event.dateEnd) <= 0)){
                    Log.d("Populate", "EventsAndDays Hosting added for ${event.name} and ${calendarToStr(day.date)}")
                    insertEventsAndDays(EventsAndDays(event.id, day.dayId))
                }
            }
        } else {
            for (day in days){
                if ((event.dateStart.compareTo(day.date) == 0) or (day.date.compareTo(event.dateEnd) == 0)){
                    Log.d("Populate", "EventsAndDays added for ${event.name} and ${calendarToStr(day.date)}")
                    insertEventsAndDays(EventsAndDays(event.id, day.dayId))
                }
            }
        }
    }

    // EXPENSES
    fun getEventByIdWithExpenses(event_id: String) = mExpenseDAO.getEventByIdWithExpenses(event_id)
    fun getExpensesOfTripName(tripid: String) = mExpenseDAO.getExpensesOfTripName(tripid)
    fun getAllExpenses() = mExpenseDAO.getAllExpenses()
    fun totalExpensesOfTrip(tripid: String) = mExpenseDAO.totalExpensesOfTrip(tripid)
    fun insertExpense(expense: Expense) {
        insertExpenseAsyncTask(mExpenseDAO).execute(expense)
    }
    fun deleteExpenseById(id: String) {
        deleteExpenseAsyncTask(mExpenseDAO).execute(id)
    }

    private class insertExpenseAsyncTask (private val mAsyncExpenseDao: ExpenseDAO) : AsyncTask<Expense, Void, Void>() {
        override fun doInBackground(vararg params: Expense): Void? {
            mAsyncExpenseDao.insertExpense(params[0])
            return null
        }
    }

    private class deleteExpenseAsyncTask (private val mAsyncExpenseDao: ExpenseDAO) : AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg params: String): Void? {
            mAsyncExpenseDao.deleteExpenseById(params[0])
            return null
        }
    }

    fun insertAllElements(all: TripsList.AllElements){
        insertAllElementsAsyncTask(mTripDAO, mEventDAO, mEventsAndDaysDAO, mExpenseDAO, mTripDayDAO)
                .execute(all)
    }

    private class insertAllElementsAsyncTask (
            private val mAsyncTripDao: TripDAO,
            private val mAsyncEventDao: EventDAO,
            private val mAsyncEventsAndDaysDao: EventsAndDaysDAO,
            private val mAsyncExpenseDao: ExpenseDAO,
            private val mAsyncTripDayDao: TripDayDAO
    ) : AsyncTask<TripsList.AllElements, Void, Void>() {
        override fun doInBackground(vararg params: TripsList.AllElements): Void? {
            val all = params[0]
            mAsyncTripDao.insertAllTrips(all.trips)
            mAsyncEventDao.insertAllEvents(all.events)
            mAsyncTripDayDao.insertAllTripDays(all.tripDays)
            mAsyncExpenseDao.insertAllExpenses(all.expenses)
            mAsyncEventsAndDaysDao.insertAllEventsAndDays(all.eventsAndDays)
            Log.d("Help", "insertAllElements done")
            return null
        }
    }

    // ROUTES
    fun getRoutesByTripId(tripId: String) = mRouteBtwEventsDAO.getRoutesByTripId(tripId)
    fun insertRoute(element: RouteBtwEvents) {
        insertRouteAsyncTask(mRouteBtwEventsDAO).execute(element)
    }
    fun insertAllRoutes(elements: List<RouteBtwEvents>) {
        insertAllRoutesAsyncTask(mRouteBtwEventsDAO).execute(elements)
    }

    private class insertRouteAsyncTask (private val mAsyncRouteBtwEventsDAO: RouteBtwEventsDAO) :
            AsyncTask<RouteBtwEvents, Void, Void>() {
        override fun doInBackground(vararg params: RouteBtwEvents): Void? {
            mAsyncRouteBtwEventsDAO.insertRoute(params[0])
            return null
        }
    }

    private class insertAllRoutesAsyncTask (private val mAsyncRouteBtwEventsDAO: RouteBtwEventsDAO) :
            AsyncTask<List<RouteBtwEvents>, Void, Void>() {
        override fun doInBackground(vararg params: List<RouteBtwEvents>): Void? {
            mAsyncRouteBtwEventsDAO.insertAllRoutes(params[0])
            return null
        }
    }
}