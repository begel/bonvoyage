package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import java.util.*


@Entity(tableName = "events_days",
        foreignKeys = [
            ForeignKey(parentColumns = ["id"], childColumns = ["event_id"],
                onDelete = CASCADE, entity = Event::class),
            ForeignKey(parentColumns = ["day_id"], childColumns = ["day_id"],
                    onDelete = CASCADE, entity = TripDay::class)],
        indices = [Index("event_id"), Index("day_id")])
data class EventsAndDays(
        @ColumnInfo(name = "event_id") val eventId: String,
        @ColumnInfo(name = "day_id") val dayId: String,
        @PrimaryKey @ColumnInfo(name = "id") val id: String = UUID.randomUUID().toString()
)