package fr.begel.apps.bonvoyage.ui

import android.Manifest
import android.app.DialogFragment
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.graphics.PointF
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.here.android.mpa.common.*
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.RouteManager
import com.here.android.mpa.routing.RouteOptions
import com.here.android.mpa.routing.RoutePlan
import com.here.android.mpa.routing.RouteResult
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.persistence.*
import kotlinx.android.synthetic.main.activity_here_maps.*
import kotlin.collections.ArrayList


class HereMaps : AppCompatActivity(),
    HereSelectDateDialogFragment.HereSelectDateDialogListener{
    private val LOG_TAG = HereMaps::class.java.simpleName

    // permissions request code
    private val REQUEST_CODE_ASK_PERMISSIONS = 1

    /**
     * Permissions that need to be explicitly requested from end user.
     */
    private val REQUIRED_SDK_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE)

    // map embedded in the map fragment
    private var map: Map? = null

    // map fragment embedded in this activity
    private lateinit var mapFragment: MapFragment

    private var rm: RouteManager = RouteManager()

    private lateinit var viewModel: EventViewModel
    private var days : ArrayList<TripDayWithEvents> = ArrayList()
    private var selectedDays : ArrayList<TripDayWithEvents> = ArrayList()
    private var allEventsMap: HashMap<String, Event> = HashMap()
    private var daysWithEventsId: List<TripDayWithEventsId> = emptyList()
    private var markers: MutableList<MapMarker> = mutableListOf()
    private var routes: MutableList<MapRoute> = mutableListOf()
    private var routesCoordinate: MutableList<Pair<GeoCoordinate, GeoCoordinate>> = mutableListOf()
    private var center: GeoCoordinate = GeoCoordinate(40.7787604,-74.0355826,10.75)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val tripId = intent.getStringExtra(TRIP_ID)!!
        val viewModelFactory = Injection.provideViewModelFactory(applicationContext!!)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)

        viewModel.getTripDaysWithEventsId(tripId).observe({-> this.lifecycle}){it ->
            daysWithEventsId = it ?: emptyList()
            days = OrderTripDaysWithEvents(allEventsMap, daysWithEventsId).days
            selectedDays = days.clone() as ArrayList<TripDayWithEvents>
            updateMap()
        }
        viewModel.getEventsOfTrip(tripId).observe (
                { -> this.lifecycle},
                {events: List<Event>? ->
                    if (events != null){
                        allEventsMap = HashMap()
                        for (event in events) {
                            allEventsMap.put(event.id, event)
                        }
                        days = OrderTripDaysWithEvents(allEventsMap, daysWithEventsId).days
                        selectedDays = days.clone() as ArrayList<TripDayWithEvents>
                        updateMap()
                    }
                }
        )
        //testSaveDb()
    }

    private fun initialize() {
        setContentView(R.layout.activity_here_maps)

        // Search for the map fragment to finish setup by calling init().
        mapFragment = fragmentManager.findFragmentById(R.id.mapfragment) as MapFragment
        mapFragment.init { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                mapFragment.mapGesture.addOnGestureListener(OnGestureListener())
                // retrieve a reference of the map from the map fragment
                map = mapFragment.map
                // Set the map center to the Vancouver region (no animation)
                //map!!.setCenter(GeoCoordinate(40.7787604,-74.0355826,10.75),
                map!!.setCenter(center,
                        Map.Animation.NONE)
                // Set the zoom level to the average between min and max
                map!!.zoomLevel = (map!!.maxZoomLevel + map!!.minZoomLevel) / 2
                updateMap()
            } else {
                Log.e(LOG_TAG, "Cannot initialize MapFragment (${error.details})")
            }
        }

        // Date Button
        map_button_calendar.setOnClickListener{_ -> selectDays()}
    }

    private fun addMarkerToMap(coordinate: GeoCoordinate, name: String){
        val pin = Image()
        pin.setBitmap(getBitmapFromVectorDrawable(getDrawable(R.drawable.ic_location_on_black_24dp)))
        val marker = MapMarker(coordinate, pin)
        marker.title = name
        map!!.addMapObject(marker)
        markers.add(marker)
    }

    //REF: https://jonnyzzz.com/blog/2017/03/01/guarded-by-lock/
    /*class GuardedByLock<out L: Lock, out T>(
            val lock: L,
            val state: T
    ) {
        inline fun <Y> runWithLock(action: T.() -> Y) = lock.withLock { state.action() }
    }*/

    private fun updateMap(){
        if (map == null) return
        val mapObjects = mutableListOf<MapObject>().apply {
            addAll(markers)
            addAll(routes)
        }
        map!!.removeMapObjects(mapObjects)
        markers = mutableListOf()
        for (day in selectedDays){
            val coordinates = mutableListOf<GeoCoordinate>()
            for (event in day.events){
                if (event.coordinate != null) {
                    addMarkerToMap(event.coordinate, event.name + "\n${event.address}")
                    coordinates.add(event.coordinate)
                }
            }
            for (j in 0..coordinates.size - 2){
                routesCoordinate.add(coordinates[j] to coordinates[j+1])
            }
        }
        val box = GeoBoundingBox
                .getBoundingBoxContainingGeoCoordinates(markers.map { m -> m.coordinate })
        if (box != null){
            map!!.zoomTo(box, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION)
        }
        val calculateNextRoute = CalculateNextRoute(routesCoordinate, map, rm, routes)
        calculateNextRoute.afterCalculateRoute(null)
    }

    private class CalculateNextRoute(val routesCoordinate: MutableList<Pair<GeoCoordinate, GeoCoordinate>>,
                                     var map: Map?, var rm: RouteManager, var routes: MutableList<MapRoute>):
            RouteListener.AfterCalculateRouteListener {
        override fun afterCalculateRoute(mapRoute: MapRoute?) {
            if (mapRoute != null) {
                routes.add(mapRoute)
                map!!.addMapObject(mapRoute)
            }
            if (routesCoordinate.isNotEmpty()){
                val first = routesCoordinate.removeAt(0)
                addRoute(first.first, first.second)
            }
        }
        private fun addRoute(source: GeoCoordinate, destination: GeoCoordinate){
            Log.d("Help", "addRoute between $source and $destination")
            val routeOptions = RouteOptions().apply {
                transportMode = RouteOptions.TransportMode.CAR
                routeType = RouteOptions.Type.FASTEST
            }
            val routePlan = RoutePlan()
            routePlan.routeOptions = routeOptions
            routePlan.addWaypoint(source)
            routePlan.addWaypoint(destination)
            val rl = RouteListener()
            rl.setListener(this)
            rm.calculateRoute(routePlan, rl)
        }
    }

    // Selection of dates to display
    // Delete Expenses
    fun selectDays() {
        HereSelectDateDialogFragment
                .newInstance(days as ArrayList, selectedDays as ArrayList)
                .show(fragmentManager, "select_date")
    }

    override fun onSelectDateDialogPositiveClick(dialog: DialogFragment, tripDays: ArrayList<TripDayWithEvents>) {
        selectedDays = tripDays
        updateMap()
    }

    // Bubble on Click
    private class OnGestureListener: MapGesture.OnGestureListener{
        private var markerBubble: MapMarker? = null

        override fun onDoubleTapEvent(p0: PointF?): Boolean {
            return false
        }

        override fun onLongPressEvent(p0: PointF?): Boolean {
            return false
        }

        override fun onLongPressRelease() {
            return
        }

        override fun onMapObjectsSelected(p0: MutableList<ViewObject>?): Boolean {
            if (p0 == null) return false
            for (viewObject in p0){
                if (viewObject.baseType == ViewObject.Type.USER_OBJECT){
                    val marker = viewObject as? MapMarker
                    if (marker == null) return false
                    marker.showInfoBubble()
                    markerBubble = marker
                    return true
                }
            }
            return false
        }

        override fun onMultiFingerManipulationEnd() {
            return
        }

        override fun onMultiFingerManipulationStart() {
            return
        }

        override fun onPanEnd() {
            return
        }

        override fun onPanStart() {
            return
        }

        override fun onPinchLocked() {
            return
        }

        override fun onPinchZoomEvent(p0: Float, p1: PointF?): Boolean {
            return false
        }

        override fun onRotateEvent(p0: Float): Boolean {
            return false
        }

        override fun onRotateLocked() {
            return
        }

        override fun onTapEvent(p0: PointF?): Boolean {
            if (markerBubble == null){
                return false
            } else {
                markerBubble!!.hideInfoBubble()
                return true
            }
        }

        override fun onTiltEvent(p0: Float): Boolean {
            return false
        }

        override fun onTwoFingerTapEvent(p0: PointF?): Boolean {
            return false
        }
    }

    private class RouteListener: RouteManager.Listener {
        override fun onCalculateRouteFinished(error: RouteManager.Error?, results: MutableList<RouteResult>?) {
            Log.d("Help", "Calculate route finished")
            if (error == RouteManager.Error.NONE){
                mListener!!.afterCalculateRoute(MapRoute(results!!.first().route))
            } else {
                Log.d("Help", "onCalculateRouteFinished had an error ${error.toString()}")
            }
        }
        override fun onProgress(p0: Int) {
            Log.d("Help", "onProgress routeListener $p0")
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        private var mListener: AfterCalculateRouteListener? = null
        fun setListener(listener: AfterCalculateRouteListener) {mListener = listener}

        interface AfterCalculateRouteListener {
            fun afterCalculateRoute(mapRoute: MapRoute?)
        }
    }
}