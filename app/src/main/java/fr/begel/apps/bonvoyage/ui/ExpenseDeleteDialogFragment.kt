package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.persistence.Expense
import java.util.*
import kotlin.collections.ArrayList

const val EXPENSE_DELETE_CHOICES = "fr.begel.apps.bonvoyage.ui.delete_expenses"
class ExpenseDeleteDialogFragment: DialogFragment() {

    private lateinit var mSelectedItems: ArrayList<Expense>
    var mListener: ExpenseDeleteDialogListener? = null
    private lateinit var choices: ArrayList<Expense>

    // Interface to link the implemented function to the mListener
    interface ExpenseDeleteDialogListener {
        fun onExpenseDeleteDialogPositiveClick(dialog: DialogFragment, expenses : ArrayList<Expense>);
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as ExpenseDeleteDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity.toString() + " must implement NewExpenseDialogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        mSelectedItems = ArrayList();  // Where we track the selected items
        // Argument
        arguments?.let {
            choices = it.getSerializable(EXPENSE_DELETE_CHOICES) as ArrayList<Expense>
        }
        val strings = choices.map {
            expense ->
            val symbol = Currency.getInstance(expense.currency).symbol
            val date = calendarToStr(expense.date)
            "${expense.amount}$symbol le $date" as CharSequence
        }
        val charSequences = Array(strings.size, {i -> strings[i]})
        val builder = AlertDialog.Builder(activity);
        // Build the Dialog
        builder.setTitle(R.string.delete)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setMultiChoiceItems(charSequences, null)
                        {dialogInterface, which, isChecked ->
                            val expense = choices[which]
                            if (isChecked) {
                                mSelectedItems.add(expense);
                            } else if (mSelectedItems.contains(expense)) {
                                mSelectedItems.remove(expense)
                            }
                        }
                // Set the action buttons
                .setPositiveButton(R.string.ok) { dialog, id ->
                    mListener!!.onExpenseDeleteDialogPositiveClick(this, mSelectedItems)
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                }
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(choices: ArrayList<Expense>) =
                ExpenseDeleteDialogFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(EXPENSE_DELETE_CHOICES, choices)
                    }
                }
    }
}