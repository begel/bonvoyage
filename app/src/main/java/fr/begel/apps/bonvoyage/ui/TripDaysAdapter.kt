package fr.begel.apps.bonvoyage.ui

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.*
import fr.begel.apps.bonvoyage.persistence.*

class TripDaysAdapter(private var tripWithDays: TripWithDays?) :
        RecyclerView.Adapter<TripDaysAdapter.ViewHolder>() {

    private var mClickListener: MyItemClickListener? = null
    private var mLongClickListener: MyItemLongClickListener? = null
    private var days = tripWithDays?.sortedDays()
    private var dayStr = ""
    // Provide a reference to the views for each data item_trip
    // Complex data items may need more than one view per item_trip, and
    // you provide access to all the views for a data item_trip in a view holder.
    // Each data item_trip is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View,
                     val mClickListener : MyItemClickListener?,
                     val mLongClickListener: MyItemLongClickListener?) :
            RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        val dayView: TextView = view.findViewById(R.id.item_trip_day_nb) as TextView
        val dateView: TextView = view.findViewById(R.id.item_trip_day_date) as TextView
        val nameView: TextView = view.findViewById(R.id.item_trip_day_name) as TextView

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view: View) {
            if(mClickListener != null){
                mClickListener.onItemClick(view,adapterPosition);
            }
        }

        override fun onLongClick(view: View): Boolean {
            if(mLongClickListener != null){
                mLongClickListener.onItemLongClick(view,adapterPosition);
            }
            return true
        }
    }

    // Create new views (invoked by the view manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): TripDaysAdapter.ViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_trip_day_main, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and view parameters
        dayStr = parent.context.resources.getString(R.string.day)
        return ViewHolder(layout, mClickListener, mLongClickListener)
    }

    // Replace the contents of a view (invoked by the view manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if (days == null) return
        val day = days!![position]
        holder.nameView.text = day.name
        holder.dateView.text = calToShortText(day.date)
        val pos = dayStr + " " + (position + 1)
        holder.dayView.text = pos
    }

    // Return the size of your dataset (invoked by the view manager)
    override fun getItemCount() = days?.size ?: 0

    fun setTrip(trip: TripWithDays) {
        tripWithDays = trip
        days = trip.sortedDays()
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(mClickListener: MyItemClickListener?){
        this.mClickListener = mClickListener
    }

    fun setOnItemLongClickListener(mLongClickListener: MyItemLongClickListener?){
        this.mLongClickListener = mLongClickListener
    }
}