package fr.begel.apps.bonvoyage.ui

import android.app.DialogFragment
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.R.id.add_expense
import fr.begel.apps.bonvoyage.R.id.trip_map
import fr.begel.apps.bonvoyage.persistence.Event
import fr.begel.apps.bonvoyage.persistence.Expense
import fr.begel.apps.bonvoyage.persistence.Trip
import java.util.*

class TripMain : AppCompatActivity(),
        NewExpenseDialogFragment.NewExpenseDialogListener,
        ExpenseDialogFragment.ExpenseDialogListener,
        RangeDateDialogFragment.RangeDateDialogListener,
        DaysOfTrip.OnDayClickListener {

    internal lateinit var tripid: String
    private var trip: Trip? = null
    private var events : List<Event>? = null
    private lateinit var mTripPagerAdapter: TripPagerAdapter
    private lateinit var mViewPager: ViewPager
    private lateinit var mTabLayout: TabLayout
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventViewModel

    private var expenseAmount: Float? = null
    private var expenseCurrency: String = Currency.getInstance(local).currencyCode
    private var expenseName: String? = null
    private var expenseEventId: String? = null

    private lateinit var mFragment2: EventsOfTrip

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_main)
        setSupportActionBar(findViewById(R.id.trip_main_toolbar))
        tripid = intent.getStringExtra(SUMMARY_TRIP_ID)
        mFragment2 = EventsOfTrip.newInstance(tripid, "")
        mTripPagerAdapter = TripPagerAdapter(supportFragmentManager, tripid, mFragment2)
        mViewPager = findViewById(R.id.trip_pager)
        mViewPager.adapter = mTripPagerAdapter
        mViewPager.offscreenPageLimit = 0
        mTabLayout = findViewById(R.id.sliding_tabs)
        mTabLayout.setupWithViewPager(mViewPager)
        mViewPager.currentItem = 1
        // Room
        viewModelFactory = Injection.provideViewModelFactory(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)
        viewModel.getTripById(tripid).observe (
                { -> this.lifecycle},
                {trip: Trip? ->
                    this.trip = trip}
        )
        trip = viewModel.getTripById(tripid).value
        viewModel.getEventsOfTrip(tripid).observe (
                { -> this.lifecycle},
                {events: List<Event>? ->
                    this.events = events
                }
        )
    }

    class TripPagerAdapter(fm: FragmentManager, val tripid: String, val mFragment2: EventsOfTrip) : FragmentPagerAdapter(fm) {

        private val titles = ArrayList<CharSequence>().apply {
            add("Dépenses")
            add("Journées")
            add("Programme")
        }

        override fun getCount(): Int  = 3

        override fun getItem(i: Int): Fragment? {
            return when (i) {
                0 -> ExpensesOfTrip.newInstance(tripid)
                1 ->  DaysOfTrip.newInstance(tripid)
                2 ->  mFragment2
                else -> null
            }
        }

        override fun getPageTitle(position: Int): CharSequence {
            return titles[position]
        }
    }
    //MENU
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_events_of_trip, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item_event clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()
        when (id) {
            add_expense -> displaySpinnerDialog()
            trip_map -> displayTripMap()
        }
        return if (id == add_expense){
            true
        } else super.onOptionsItemSelected(item)
    }

    // Spinner Dialog
    private fun displaySpinnerDialog(){
        val spinnerDialogFragment =
                NewExpenseDialogFragment.newInstance(
                        ArrayList(
                                events?.map {event -> event.id to event.name})
                )
        spinnerDialogFragment.show(fragmentManager, "new_expense_1")
    }

    //Listener Spinner Dialog
    override fun newExpenseDialogPositiveClick(dialog: DialogFragment, eventId: String?, name: String) {
        expenseEventId = eventId
        expenseName = name
        displayAmoutDialog()
    }

    // Amount Dialog
    private fun displayAmoutDialog(){
        ExpenseDialogFragment().show(fragmentManager, "new_expense_2")
    }

    override fun onExpenseDialogPositiveClick(dialog: DialogFragment, amount: Float, currency: String) {
        expenseAmount = amount
        expenseCurrency = currency
        displayDateDialog()
    }

    // DateDialog
    fun displayDateDialog() {
        val lastDate = trip!!.last_date.clone() as Calendar
        lastDate.add(Calendar.MONTH, 2)
        val firstDate = trip!!.first_date.clone() as Calendar
        firstDate.add(Calendar.YEAR, -5)
        val dateDialogFragment =
                RangeDateDialogFragment.newInstance(
                        calendarToDate(firstDate),
                        calendarToDate(lastDate),
                        ArrayList<Date>().apply{
                            add(calendarToDate(Calendar.getInstance()))
                        }
                )
        dateDialogFragment.show(fragmentManager, "new_expense3")
    }

    // DateDialog Listener
    // Create the expense at validation click
    override fun onDialogPositiveClick(dialog: DialogFragment, dates: MutableList<Date>) {
        val expense = Expense(
                tripId = trip!!.id,
                eventId = expenseEventId,
                amount = expenseAmount!!,
                currency = expenseCurrency,
                name = expenseName,
                date = dateToCalendar(dates.first()) )
        viewModel.insertExpense(expense)
        Toast.makeText(applicationContext, "New Expense de $expenseAmount in $expenseCurrency", Toast.LENGTH_LONG).show()
    }

    // Display Map of the trip
    fun displayTripMap(){
        val intent = Intent(applicationContext, HereMaps::class.java)
        intent.putExtra(TRIP_ID, tripid)
        startActivity(intent)
    }

    // Click on a day in DaysOfTrip - go to the Schedule tab
    override fun onDayClick(dayId: String) {
        Log.d("Help", "On Click: $dayId")
        mFragment2.toScroll = dayId
        mFragment2.updateDataSet()
        mViewPager.currentItem = 2
    }
}
