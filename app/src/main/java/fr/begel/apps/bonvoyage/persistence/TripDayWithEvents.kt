package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import fr.begel.apps.bonvoyage.ui.diffCalendar
import fr.begel.apps.bonvoyage.ui.hourToInt
import java.util.Calendar
import kotlin.collections.ArrayList

class TripDayWithEventsId {
    @Embedded
    val tripDay: TripDay
    @Relation(parentColumn = "day_id",
            entityColumn = "day_id",
            entity = EventsAndDays::class
            //, projection = ["event_id"]
    )
    var eventsId: List<EventsAndDays>

    constructor(tripDay: TripDay){
        this.tripDay = tripDay
        this.eventsId = arrayListOf()
    }
}

class TripDayWithEvents(
    val tripDay: TripDay,
    val events: List<Event>
)

class OrderTripDaysWithEvents(private val allEventsMap: HashMap<String, Event>,
                              private val daysWithEventsId: List<TripDayWithEventsId>) {


    val days: ArrayList<TripDayWithEvents> = associateTripDaysAndEvents()

    private fun associateTripDaysAndEvents(): ArrayList<TripDayWithEvents> {
        if (daysWithEventsId.isEmpty() or allEventsMap.isEmpty()) return arrayListOf()
        val res = arrayListOf<TripDayWithEvents>()
        for (tripDay in daysWithEventsId){
            val td = TripDayWithEvents(
                    tripDay.tripDay,
                    tripDay.eventsId.map { id -> allEventsMap.get(id.eventId)!! }
            )
            res.add(orderEvents(td))
        }
        return res
    }

    private fun orderEvents(tripDayWithEvents: TripDayWithEvents): TripDayWithEvents{
        val events = tripDayWithEvents.events
        val tripDay = tripDayWithEvents.tripDay
        val res = arrayListOf<Pair<Event, Int>>()
        val hosting = events.filter { e -> e.eventType == EventType.HOSTING }
        if (hosting.isNotEmpty())
            if ((hosting.first().dateStart.compareTo(tripDay.date) < 0)
                    and (hosting.first().dateEnd.compareTo(tripDay.date) > 0 )){
                res.add(hosting.first() to 2359)
            }
        res.addAll(events.map {e -> e to valueEvent(e, tripDay.date) })
        val tmp = res.sortedBy { e -> e.second }.map { e -> e.first }
        return TripDayWithEvents(tripDay, tmp)
    }

    private fun valueEvent(event: Event, day: Calendar): Int {
        when (event.eventType){
            EventType.HOSTING -> {
                if (diffCalendar(event.dateStart, day) == 0L) {
                    if (event.timeStartPlanned == null) return 2359
                    return hourToInt(event.timeStartPlanned)
                }
                if (diffCalendar(event.dateEnd, day) == 0L) {
                    if (event.timeEndPlanned == null) return 0
                    return hourToInt(event.timeEndPlanned)
                }
                return 0
            }
            else -> {
                if (diffCalendar(event.dateEnd, day) == 0L){
                    return hourToInt(event.timeEndPlanned ?: event.timeEnd!!)
                }
                return hourToInt(event.timeStartPlanned ?: event.timeStart!!)
            }
        }
    }
}