package fr.begel.apps.bonvoyage.ui

import android.os.Bundle
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.MyItemClickListener
import fr.begel.apps.bonvoyage.listeners.MyItemLongClickListener
import fr.begel.apps.bonvoyage.persistence.*


class DaysOfTrip : Fragment(),
        MyItemLongClickListener, MyItemClickListener
{
    private var trip: TripWithDays? = null
    private var days: List<TripDay> = emptyList()
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventViewModel
    private lateinit var viewAdapter: TripDaysAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerView: RecyclerView
    private var mListener: OnDayClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var tripId: String? = null
        arguments?.let {
            tripId = it.getString(TRIP_ID)!!
        }
        viewModelFactory = Injection.provideViewModelFactory(context!!)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)
        viewModel.getTripWithDays(tripId!!).observe (
                { -> this.lifecycle},
                {trip: TripWithDays? ->
                    this.trip = trip
                    if (trip != null) {
                        this.days = trip.sortedDays()
                        viewAdapter.setTrip(trip)
                    }
                }
        )
        trip = viewModel.getTripWithDays(tripId!!).value
        days = trip?.sortedDays() ?: emptyList()

        viewManager = LinearLayoutManager(context)
        viewAdapter = TripDaysAdapter(trip)
        viewAdapter.setOnItemClickListener(this)
        viewAdapter.setOnItemLongClickListener(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_days_of_trip, container, false)
        recyclerView = view.findViewById<RecyclerView>(R.id.days_recycler_view).apply{
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        return view
    }

    interface OnDayClickListener {
        fun onDayClick(dayId: String)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            mListener = context as OnDayClickListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement NewExpenseDialogListener")
        }
    }

    // Clicking on a Activity
    override fun onItemClick(view: View, position: Int) {
        val day = days.get(position)
        mListener!!.onDayClick(day.dayId)
    }

    // Long Click on a Activity
    override fun onItemLongClick(view: View, position: Int) {
        /*
        val event = events!!.get(position)
        Toast.makeText(view.context, event.name + " long click", Toast.LENGTH_SHORT).show()
        */
    }

    companion object {
        @JvmStatic
        fun newInstance(tripId: String) =
                DaysOfTrip().apply {
                    arguments = Bundle().apply {
                        putString(TRIP_ID, tripId)
                    }
                }
    }
}
