package fr.begel.apps.bonvoyage.ui

import android.content.Context
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.search.AutoSuggest
import com.here.android.mpa.search.AutoSuggestPlace
import com.here.android.mpa.search.ErrorCode
import com.here.android.mpa.search.TextAutoSuggestionRequest
import fr.begel.apps.bonvoyage.R
import java.util.*

const val MAX_RESULT = 5
//Ref: http://makovkastar.github.io/blog/2014/04/12/android-autocompletetextview-with-suggestions-from-a-web-service/
class AddressFinderAdapter(private val mContext: Context):BaseAdapter(), Filterable {
    private var resultList: List<AutoSuggest> = arrayListOf()
    private var ready = false
    private val mapEngine: MapEngine = MapEngine.getInstance()

    init{mapEngine.init(mContext){
        error ->
        if (error == OnEngineInitListener.Error.NONE) ready = true
        }
    }

    override fun getCount(): Int {
        return resultList.size
    }

    override fun getItem(position: Int): AutoSuggest {
        return resultList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var mConvertView = convertView
        if (convertView == null){
            val inflater =  mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            mConvertView = inflater.inflate(R.layout.item_address_finder, parent, false)
        }
        val item = getItem(position) as AutoSuggestPlace
        mConvertView!!.findViewById<TextView>(R.id.address_finder_item_name).text = item.title
        mConvertView.findViewById<TextView>(R.id.address_finder_item_address).text = item.vicinity.replace("<br/>", "\n")
        return mConvertView
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            private var filterResults: FilterResults = FilterResults()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                if (constraint != null) {
                    findPlace(constraint.toString())
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if ((results != null) and (results!!.count > 0)) {
                    resultList = results.values as List<AutoSuggest>
                    notifyDataSetChanged()
                } else
                    notifyDataSetInvalidated()
            }

            private fun findPlace(constraint: String) {
                if (ready) {
                    TextAutoSuggestionRequest(constraint).apply {
                        setSearchCenter(GeoCoordinate(41.0522372, -113.1453897))
                        val enumSet = EnumSet.copyOf(setOf(TextAutoSuggestionRequest.AutoSuggestFilterType.ADDRESS,
                                TextAutoSuggestionRequest.AutoSuggestFilterType.PLACE))
                        setFilters(enumSet)
                        setCollectionSize(MAX_RESULT)
                        execute { results, errorCode ->
                            if (errorCode == ErrorCode.NONE) {
                                filterResults.values = results
                                filterResults.count = results.size
                            }
                        }
                    }
                }
            }
        }
    }
}
