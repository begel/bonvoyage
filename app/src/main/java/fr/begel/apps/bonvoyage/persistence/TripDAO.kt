package fr.begel.apps.bonvoyage.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

/**
 * Data Access Object for the trip table.
 */
@Dao
interface TripDAO {
    /**
     * Get a trip by id.

     * @return the trip from the table with a specific id.
     */
    @Query("SELECT * FROM Trips WHERE tripid = :id")
    fun getTripById(id: String): LiveData<Trip>

    /**
     * Get all trips.
     */
    @Query("SELECT * FROM Trips ORDER BY first_date")
    fun getAllTrips(): LiveData<List<Trip>>


    /**
     * Insert a trip in the database. If the trip already exists, replace it.

     * @param trip the trip to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTrip(trip: Trip)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllTrips(elements: List<Trip>)

    /**
     * Update a trip. If the trip exists, replace it.
     * @param trip the trip to be updated.
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateTrip(trip: Trip)

    /**
     * Delete one trip by stringId.
     * @param tripid the tripid to be deleted
     */
    @Query("DELETE FROM trips where tripid = :tripId")
    fun deleteTripById(tripId: String)

    /**
     * Delete all trips.
     */
    @Query("DELETE FROM trips")
    fun deleteAllTrips()
}