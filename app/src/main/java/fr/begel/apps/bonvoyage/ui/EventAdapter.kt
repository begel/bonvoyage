package fr.begel.apps.bonvoyage.ui

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.here.android.mpa.routing.Route
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.*
import fr.begel.apps.bonvoyage.persistence.Event
import fr.begel.apps.bonvoyage.persistence.RouteBtwEvents
import fr.begel.apps.bonvoyage.persistence.TripDay
import java.util.*
import kotlin.collections.ArrayList

class EventAdapter :
        RecyclerView.Adapter<EventAdapter.ViewHolder>() {
    
    private var mClickListener: MyItemClickListener? = null
    private var mLongClickListener: MyItemLongClickListener? = null
    private var myDataset: ArrayList<Any> = arrayListOf()
    // Provide a reference to the views for each data item_trip
    // Complex data items may need more than one view per item_trip, and
    // you provide access to all the views for a data item_trip in a view holder.
    // Each data item_trip is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View,
                     val mClickListener : MyItemClickListener?,
                     val mLongClickListener: MyItemLongClickListener?) :
            RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        val item_line1: TextView = view.findViewById(R.id.item_line1) as TextView
        val item_line2: TextView = view.findViewById(R.id.item_line2) as TextView
        val item_image: ImageView = view.findViewById(R.id.item_image) as ImageView
        val item_time_start: TextView = view.findViewById(R.id.item_time_start) as TextView
        val item_time_stop: TextView = view.findViewById(R.id.item_time_end) as TextView
        val item_day_nb: TextView = view.findViewById(R.id.item_day_nb) as TextView
        val item_day_name: TextView = view.findViewById(R.id.item_day_name) as TextView
        val item_day_date: TextView = view.findViewById(R.id.item_day_date) as TextView
        val cardDay: ConstraintLayout = view.findViewById(R.id.item_day) as ConstraintLayout
        val cardEvent: ConstraintLayout = view.findViewById(R.id.item_event) as ConstraintLayout

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view: View) {
            if(mClickListener != null){
                mClickListener.onItemClick(view,adapterPosition);
            }
        }

        override fun onLongClick(view: View): Boolean {
            if(mLongClickListener != null){
                mLongClickListener.onItemLongClick(view,adapterPosition);
            }
            return true
        }
    }

    // Create new views (invoked by the view manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): EventAdapter.ViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_trip_day, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and view parameters

        return ViewHolder(layout, mClickListener, mLongClickListener)
    }

    // Replace the contents of a view (invoked by the view manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val event = myDataset[position]
        fillItem(event, holder)

    }

    // Return the size of your dataset (invoked by the view manager)
    override fun getItemCount() = myDataset.size

    fun setEvents(data: ArrayList<Any>) {
        myDataset = data
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(mClickListener: MyItemClickListener?){
        this.mClickListener = mClickListener
    }

    fun setOnItemLongClickListener(mLongClickListener: MyItemLongClickListener?){
        this.mLongClickListener = mLongClickListener
    }

    private fun fillItem(element: Any, holder: ViewHolder){
        if (element as? Event != null){
            fillEvent(element, holder)
        } else if (element as? TripDay != null){
            fillTripDay(element, holder)
        } else if (element as? RouteBtwEvents != null){
            fillRoute(element, holder)
        }
    }

    private fun fillEvent(event: Event, holder: ViewHolder){
        holder.cardEvent.visibility = View.VISIBLE
        val item = event.eventSubType
        val image = holder.view.context.getDrawable(item.drawableId)
        holder.item_image.setImageDrawable(image)
        holder.item_line1.text = item.line1(event)
        holder.item_line2.text = item.line2(event)
        holder.item_time_start.text =
                if (item.displayHour) orEmpty(calendarToHour(event.timeStartPlanned))
                else ""
        holder.item_time_stop.text =
                if (item.displayHour) orEmpty(calendarToHour(event.timeEndPlanned))
                else ""
        if (item.displayHour and (event.timeStartPlanned != null)) {
            holder.item_time_start.text = calendarToHour(event.timeStartPlanned)
        } else {
            holder.item_time_start.visibility = View.GONE
        }
        if (item.displayHour and (event.timeEndPlanned != null)){
            holder.item_time_stop.text = calendarToHour(event.timeEndPlanned)
        } else {
            holder.item_time_stop.visibility = View.GONE
        }
        holder.cardDay.visibility = View.GONE
        holder.cardDay.background = null
    }

    private fun fillTripDay(element: TripDay, holder: ViewHolder){
        holder.cardDay.visibility = View.VISIBLE
        holder.cardDay.background = holder.view.context.getDrawable(R.color.colors13)
        holder.item_day_name.text = orEmpty(element.name)
        holder.item_day_date.text = calToShortText(element.date)
        holder.item_day_nb.text = "Jour ${element.number + 1}"
        holder.cardEvent.visibility = View.GONE
    }

    private fun fillRoute(element: RouteBtwEvents, holder: ViewHolder){
        holder.cardDay.visibility = View.VISIBLE
        holder.cardDay.background = holder.view.context.getDrawable(R.color.colors50)
        holder.item_day_name.text = fromSecondToTime(element.tta)
        holder.item_day_date.text = element.mode
        holder.item_day_nb.text = ""
        holder.cardEvent.visibility = View.GONE
    }
}
