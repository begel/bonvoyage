package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import java.util.*

@Entity(tableName = "expenses",
        foreignKeys = [
            /*(ForeignKey(entity = Event::class, parentColumns = ["id"],
                childColumns = ["event_id"], onDelete = CASCADE)),*/
            (ForeignKey(entity = Trip::class, parentColumns = ["tripid"],
                    childColumns = ["trip_id"], onDelete = CASCADE))],
        indices = [Index("id"), Index("event_id"), (Index("trip_id"))])
@TypeConverters(Converters::class)
data class Expense (@ColumnInfo(name = "trip_id") val tripId: String,
                    @ColumnInfo(name = "date") val date: Calendar,
                    @ColumnInfo(name = "amount") val amount: Float,
                    @ColumnInfo(name = "currency") val currency: String,
                    @ColumnInfo(name = "name") val name: String?,
                    @ColumnInfo(name = "event_id") val eventId: String?,
                    @PrimaryKey @ColumnInfo(name = "id") val id: String = UUID.randomUUID().toString(),
                    @ColumnInfo(name = "last_modified") val lastModified : Calendar = GregorianCalendar.getInstance()
                    )

class EventWithExpenses {
    @Embedded
    var event: Event
    @Relation(parentColumn = "id", entityColumn = "event_id")
    var expenses: List<Expense>

    constructor(event: Event){
        this.event = event
        this.expenses = emptyList()
    }
}

data class ExpenseWithName (
        @ColumnInfo(name = "display") val name: String,
        @Embedded val expense: Expense
)

data class TotalExpense (
        @ColumnInfo(name = "total") val total: Float,
        @ColumnInfo(name = "currency") val currency: String
)
