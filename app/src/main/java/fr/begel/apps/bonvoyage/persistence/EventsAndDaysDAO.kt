package fr.begel.apps.bonvoyage.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface EventsAndDaysDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEventsAndDays(element: EventsAndDays)

    @Delete
    fun deleteEventsAndDays(element: EventsAndDays)

    @Query("DELETE FROM events_days WHERE event_id = :eventId")
    fun deleteEventsAndDaysOfEvent(eventId: String)

    @Query("DELETE FROM events_days WHERE day_id = :dayId")
    fun deleteEventsAndDaysOfDay(dayId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllEventsAndDays(elements: List<EventsAndDays>)

    @Query("SELECT * FROM events_days")
    fun getAllEventsAndDays(): LiveData<List<EventsAndDays>>
}