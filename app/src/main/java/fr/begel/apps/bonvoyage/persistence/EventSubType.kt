package fr.begel.apps.bonvoyage.persistence

import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.ui.*
import fr.begel.apps.bonvoyage.persistence.EventType.*

enum class EventSubType(val drawableId: Int, val stringId: Int, val displayHour: Boolean, val id: Int) {
    // Transport
    RENTACAR(R.drawable.ic_directions_car_black_24dp, R.string.rentacar, false, 30){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    },
    FLIGHT(R.drawable.ic_flight_black_24dp, R.string.flight, true, 31){
        override fun line1(event: Event) = add1_hour(event)
        override fun line2(event: Event) = add2_hour_seat(event)
    },
    BOAT(R.drawable.ic_directions_boat_black_24dp, R.string.boat, true, 32){
        override fun line1(event: Event) = add1_hour(event)
        override fun line2(event: Event) = add2_hour_seat(event)
    },
    TRAIN(R.drawable.ic_train_black_24dp, R.string.train, true, 33){
        override fun line1(event: Event) = add1_hour(event)
        override fun line2(event: Event) = add2_hour_seat(event)
    },
    BUS(R.drawable.ic_directions_bus_black_24dp, R.string.bus, true, 34){
        override fun line1(event: Event) = add1_hour(event)
        override fun line2(event: Event) = add2_hour_seat(event)
    },

    // Hosting
    AIRBNB(R.drawable.ic_hotel_black_24dp, R.string.airbnb, false, 10){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    },
    HOTEL(R.drawable.ic_hotel_black_24dp, R.string.hotel, false, 11){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    },

    // Activity
    HIKE(R.drawable.ic_directions_walk_black_24dp, R.string.hike, true, 20){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    },
    SIGHTSEEING(R.drawable.ic_location_city_black_24dp, R.string.sightseeing, true, 21){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    },
    SELFVISIT(R.drawable.ic_directions_walk_black_24dp, R.string.selfvisit, true, 22){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    },
    TOUR(R.drawable.ic_directions_walk_black_24dp, R.string.tour, true, 23){
        override fun line1(event: Event) = event.name
        override fun line2(event: Event) = orEmpty(event.address)
    };


    abstract fun line1(event: Event): String
    abstract fun line2(event: Event): String

    internal fun concatStr(str1: String?, str2: String?): String {
        var res = ""
        if (str1 != null) res = str1
        if ((str1 != null) and (str2 != null)) res += " - "
        if (str2 != null) res += str2
        return res
    }

    internal fun add1_hour(event: Event): String {
        return concatStr(calendarToHour(event.timeStart), event.address)
    }
    internal fun add2_hour_seat(event: Event): String {
        var str1: String? = concatStr(calendarToHour(event.timeEnd), event.addressEnd)
        if (str1 == "") str1 = null
        return concatStr(str1, event.seat)
    }

}