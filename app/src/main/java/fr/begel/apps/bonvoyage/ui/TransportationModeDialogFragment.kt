package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.WindowManager
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.persistence.TripDayWithEvents

const val HERE_URL = "fr.begel.apps.bonvoyage.ui.here_url"

class TransportationModeDialogFragment: DialogFragment() {

    private lateinit var url: String
    private var transpName = arrayOf("En voiture", "A pied", "A vélo", "En transport en commun")
    private var transpSuffix = arrayListOf("d", "w", "b", "pt")

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Argument
        arguments?.let {
            url = it.getString(HERE_URL)
        }
        val builder = AlertDialog.Builder(activity);
        // Build the Dialog
        builder.setTitle(R.string.transportation_mode)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setItems(transpName)
                {_, which ->
                    goToUrl(url + transpSuffix[which])
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                }
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }

    fun goToUrl(url: String){
        val uriUrl = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(intent)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(url: String) =
                TransportationModeDialogFragment().apply {
                    arguments = Bundle().apply {
                        putString(HERE_URL, url)
                    }
                }
    }
}