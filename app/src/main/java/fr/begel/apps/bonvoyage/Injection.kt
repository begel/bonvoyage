package fr.begel.apps.bonvoyage

import android.content.Context
import fr.begel.apps.bonvoyage.ui.ViewModelFactory

object Injection {


    fun provideViewModelFactory(context: Context): ViewModelFactory {
        //val dataSource = provideUserDataSource(context)
        return ViewModelFactory(context)
    }
}