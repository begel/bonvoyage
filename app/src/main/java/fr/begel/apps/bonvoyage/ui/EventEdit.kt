package fr.begel.apps.bonvoyage.ui

import android.app.Activity
import android.app.DialogFragment
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.mapping.MapMarker
import com.here.android.mpa.search.AutoSuggestPlace
import com.here.android.mpa.search.ErrorCode
import com.here.android.mpa.search.GeocodeRequest2
import com.here.android.mpa.search.GeocodeResult
import fr.begel.apps.bonvoyage.Injection
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.R.id.*
import fr.begel.apps.bonvoyage.listeners.MyGeoRequestsExecuteListener
import fr.begel.apps.bonvoyage.listeners.MyItemClickListener
import fr.begel.apps.bonvoyage.listeners.MyItemLongClickListener
import fr.begel.apps.bonvoyage.persistence.*
import kotlinx.android.synthetic.main.activity_event_edit.*
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.app_bar_layout.*
import kotlin.math.exp


const val TRIP_ID = "fr.begel.apps.bonvoyage.ui.trip_id"
const val TRIP_START_DATE = "fr.begel.apps.bonvoyage.ui.trip_start"
const val TRIP_END_DATE = "fr.begel.apps.bonvoyage.ui.trip_end_date"
const val EVENT_ID = "fr.begel.apps.bonvoyage.ui.event_id"
const val REQUEST_CODE_EVENT_DETAILS = 1
class EventEdit : AppCompatActivity(),
        RangeDateDialogFragment.RangeDateDialogListener,
        ExpenseDialogFragment.ExpenseDialogListener,
        ExpenseDeleteDialogFragment.ExpenseDeleteDialogListener,
        MyGeoRequestsExecuteListener.MListener
{

    private var tripId: String = ""
    private lateinit var eventId: String
    private var event: EventWithExpenses? = null
    private lateinit var tripStartDay: Calendar
    private lateinit var tripEndDay: Calendar
    private var dates: ArrayList<Date> = ArrayList()
    private val types = ArrayList<String>()
    internal var subTypes: ArrayList<String> = ArrayList()
    internal var subTypeId: Int? = null
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventViewModel
    private lateinit var expenseAdapter: ExpenseAdapterSmall
    private lateinit var expenseManager: LinearLayoutManager
    private var expenseAmount: Float? = null
    private var expenseCurrency: String? = null
    private val onExpenseListener = OnExpenseListener()
    private var expenses : ArrayList<Expense> = arrayListOf()
    private var days : List<TripDay>? = null
    private var coordinate: GeoCoordinate? = null
    private var coordinateEnd: GeoCoordinate? = null
    private val mListener = MyGeoRequestsExecuteListener()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_edit)
        tripId = intent.getSerializableExtra(TRIP_ID) as String
        tripStartDay = intent.getSerializableExtra(TRIP_START_DATE) as Calendar
        tripEndDay = intent.getSerializableExtra(TRIP_END_DATE) as Calendar
        eventId = intent.getSerializableExtra(EVENT_ID) as String? ?: ""
        setSupportActionBar(toolbar)
        //Listeners
        event_create_date.setOnClickListener { _ -> displayDateDialogFragment() }
        event_create_type.onItemSelectedListener = OnTypeSelected(resources, event_create_subtype, this)
        event_create_start.setOnClickListener{view -> displayTimeDialogFragment(view as EditText) }
        event_create_end.setOnClickListener{view -> displayTimeDialogFragment(view as EditText) }
        event_create_planned.setOnClickListener{view -> displayTimeDialogFragment(view as EditText) }
        event_create_planned_end.setOnClickListener{view -> displayTimeDialogFragment(view as EditText) }
        event_create_planned_checkbox.setOnClickListener{view -> onClickCheckBox(view, event_create_planned) }
        event_create_planned_end_checkbox.setOnClickListener{view -> onClickCheckBox(view, event_create_planned_end) }
        event_create_expense_add.setOnClickListener{ _ -> displayAmountDialog()}
        event_create_expense_del.setOnClickListener{_ -> deleteExpenses()}
        event_create_address.apply {
            setAdapter(AddressFinderAdapter(this.context))
            threshold = 4
            setOnItemClickListener{ adapterView, view, i, l -> onAdressClickListener(adapterView, view, i) }
        }
        event_create_address_end.apply {
            setAdapter(AddressFinderAdapter(this.context))
            threshold = 4
            setOnItemClickListener{ adapterView, view, i, l -> onAdressEndClickListener(adapterView, view, i) }
        }


        // Spinners for types
        for (t in EventType.values()) {
            types.add(resources.getString(t.stringId))
        }
        val dataAdapterType = ArrayAdapter(this, android.R.layout.simple_spinner_item, types)
        dataAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        event_create_type.setAdapter(dataAdapterType)
        subTypes = ArrayList<String>()
        for (subType in EventType.values()[0].subTypes())
            subTypes.add(applicationContext.getString(subType.stringId))
        //Access to the DB
        viewModelFactory = Injection.provideViewModelFactory(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)
        if (eventId.isNotEmpty()) {
            viewModel.getEventByIdWithExpenses(eventId).observe (
                    { -> this.lifecycle})
            {eventWithExpenses: EventWithExpenses? ->
                event = eventWithExpenses
                if (eventWithExpenses != null){
                    expenses = eventWithExpenses.expenses as ArrayList<Expense>
                    displayEventFields()
                }}
            event = viewModel.getEventByIdWithExpenses(eventId).value
            displayEventFields()
        } else {
            eventId = UUID.randomUUID().toString()
        }
        viewModel.getTripDays(tripId).observe(
                { -> this.lifecycle})
        {days: List<TripDay>? ->
            this.days = days
        }
        //List of expenses
        expenseManager = LinearLayoutManager(this)
        expenseAdapter = ExpenseAdapterSmall(emptyList())
        expenseAdapter.setOnItemClickListener(onExpenseListener)
        expenseAdapter.setOnItemLongClickListener(onExpenseListener)
        event_create_expense_list.layoutManager = expenseManager
        event_create_expense_list.adapter = expenseAdapter
    }

    private fun displayDateDialogFragment(){
        val dialogFragment = RangeDateDialogFragment()
        val args = Bundle()
        val endDay = tripEndDay.clone() as Calendar
        endDay.add(Calendar.DATE, 1)
        args.putSerializable(ARG_DATE_START, calendarToDate(tripStartDay))
        args.putSerializable(ARG_DATE_END, calendarToDate(endDay))
        args.putSerializable(ARG_DATES_SELECTED, dates)
        dialogFragment.arguments = args
        dialogFragment.show(fragmentManager, "new_event_date")
    }

    // Date Dialog Listener
    override fun onDialogPositiveClick(dialog: DialogFragment, dates: MutableList<Date>) {
        this.dates = ArrayList(dates)
        when (dialog.tag) {
            "new_event_date" -> {
                val res = when (dates.size) {
                    0 -> ""
                    1 -> dateToText(dates.first())
                    else -> dateToText(dates.first()) + " - "  + dateToText(dates.last())
                }
                event_create_date.setText(res)}
            "new_expense2" -> {
                addNewExpense(dates.first())
            }
        }


    }

    // Listener when type is selected - display the correct subtypes
    private class OnTypeSelected(private val res: Resources, private val spinner: Spinner,
                                 private val parent: AppCompatActivity): AdapterView.OnItemSelectedListener {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            val ec = parent as EventEdit
            ec.subTypes = ArrayList<String>()
            for (subType in EventType.values()[p2].subTypes())
                ec.subTypes.add(res.getString(subType.stringId))
            val dataAdapterSubType = ArrayAdapter(parent, android.R.layout.simple_spinner_item, ec.subTypes)
            dataAdapterSubType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.setAdapter(dataAdapterSubType)
            if (ec.subTypeId != null) {
                ec.event_create_subtype.setSelection(ec.subTypes.indexOf(res.getString(ec.subTypeId!!)))
                ec.subTypeId = null
            }
        }

        override fun onNothingSelected(p0: AdapterView<*>?) {
        }
    }

    // Display the date dialog for the event's date
    private fun displayTimeDialogFragment(view: EditText){
        val hour = textToHour(view.text.toString())
        val dialogFragment = TimeDialogFragment
                .newInstance(view.id, hour.first, hour.second)
        dialogFragment.show(fragmentManager, "time_picker")
    }

    // Checkbox hide a view
    private fun onClickCheckBox(view: View, toHideView: View){
        val checked = (view as CheckBox).isChecked
        if (checked)
            toHideView.visibility = View.GONE
        else
            toHideView.visibility = View.VISIBLE
    }

    //MENU
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_validate, menu)
        return true
    }

    // Select item in the Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item_event clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()
        when (id) {
            event_create_ok -> saveEvent()
            event_create_cancel -> finish()
        }
        return if ((id == event_create_cancel) or (id == event_create_ok) ){
            true
        } else super.onOptionsItemSelected(item)
    }
    // Request GeoCoordinate
    private fun requestGeoCoordinate(query: String, start: Boolean){
        if (query.isEmpty()) return
        val geocodeRequest = GeocodeRequest2(query)
        geocodeRequest.setSearchArea(GeoCoordinate(39.380141,-98.3252068), 4000000)
        geocodeRequest.execute { results: List<GeocodeResult>, errorCode: ErrorCode ->
            if (errorCode == ErrorCode.NONE) {
                if (results.isNotEmpty()) {
                    Log.d("help", "Results: ${results.size}")
                    val res = results.first()
                    mListener.onRequestEnd(start to res.location.coordinate)
                    Log.d("Help", "Query: $query | Address found: ${res.location.address.text}")
                }
            }
        }
    }

    private fun saveEvent(){
        val requestAddress = (event_create_address.text.toString().isNotEmpty()
                and ((event_create_address.text.toString() != event?.event?.address) or
                (event?.event?.coordinate == null)))
        val requestAddressEnd = (event_create_address_end.text.toString().isNotEmpty()
                and ((event_create_address_end.text.toString() != event?.event?.addressEnd) or
                (event?.event?.coordinate_end == null)))
        val m = (if (requestAddress) 1 else 0) + (if (requestAddressEnd) 1 else 0)
        if (m > 0){
            mListener.setExpected(m)
            mListener.setListener(this)
            MapEngine.getInstance().init(applicationContext){
                error ->
                if (error == OnEngineInitListener.Error.NONE) {
                    if (requestAddress)
                        requestGeoCoordinate(event_create_address.text.toString(), true)
                    if (requestAddressEnd)
                        requestGeoCoordinate(event_create_address_end.text.toString(), false)
                }
            }
        } else {
            createEvent()
        }
    }

    override fun onGeoRequestsEnd(list: MutableList<Pair<Boolean, GeoCoordinate>>) {
        for (e in list){
            if (e.first) coordinate = e.second
            else coordinateEnd = e.second
            createEvent()
        }
    }


    // Create the event
    private fun createEvent(){
        val start = viewHourToCalendar(event_create_start)
        val end = viewHourToCalendar(event_create_end)
        var planned_start = start
        var planned_end = end
        if (event_create_planned_checkbox.isChecked)
            planned_start = viewHourToCalendar(event_create_planned)
        if (event_create_planned_end_checkbox.isChecked)
            planned_end = viewHourToCalendar(event_create_planned_end)
        val typePosition = event_create_type.selectedItemPosition
        val type = EventType.values()[typePosition]
        val subTypes = type.subTypes()
        val subTypePosition = event_create_subtype.selectedItemPosition
        // Check
        if (!event_create_name.text.toString().isEmpty() and (dates.size > 0)) {
            val event = Event(
                    name = event_create_name.text.toString(),
                    tripId = tripId,
                    eventType = type,
                    eventSubType = subTypes[subTypePosition] ,
                    dateStart = dateToCalendar(dates.first()),
                    dateEnd = dateToCalendar(dates.last()),
                    timeStart = start,
                    timeStartPlanned = planned_start,
                    timeEnd = end,
                    timeEndPlanned = planned_end,
                    address = nullIfEmpty(event_create_address),
                    coordinate = coordinate,
                    addressEnd = nullIfEmpty(event_create_address_end),
                    coordinate_end = coordinateEnd,
                    note = nullIfEmpty(event_create_notes),
                    reference = nullIfEmpty(event_create_ref),
                    phone = nullIfEmpty(event_create_phone),
                    seat = nullIfEmpty(event_create_seat),
                    bookingId = nullIfEmpty(event_create_booking),
                    id = eventId
            )
            viewModel.insertEvent(event, days !!)
            for (expense in expenses){
                viewModel.insertExpense(expense)
            }
            finish()
        } else{
            val text = (
                    if (dates.size == 0) "Veuillez définir une date pour votre activité"
                    else "Veuillez donner un nom à votre évènement")
            Toast.makeText(this, text, Toast.LENGTH_LONG).show()
        }
    }

    // Display the Event is already existing
    private fun displayEventFields() {
        if (event != null) {
            val event = this.event!!.event
            var date = calendarToText(event.dateStart)
            dates.add(calendarToDate(event.dateStart))
            if (event.dateStart != event.dateEnd) {
                date = date + " - " + calendarToText(event.dateEnd)
                dates.add(calendarToDate(event.dateEnd))
            }
            if (event.timeStart != event.timeStartPlanned)
                event_create_planned_checkbox.isChecked = false
            if (event.timeEnd != event.timeEndPlanned)
                event_create_planned_end_checkbox.isChecked = false
            event_create_name.setText(event.name)
            event_create_date.setText(date)
            event_create_start.setText(calendarToHour(event.timeStart))
            event_create_planned.setText(calendarToHour(event.timeStartPlanned))
            event_create_end.setText(calendarToHour(event.timeEnd))
            event_create_planned_end.setText(calendarToHour(event.timeEndPlanned))
            event_create_address.setText(event.address)
            event_create_address_end.setText(event.addressEnd)
            event_create_phone.setText(event.phone)
            event_create_booking.setText(event.bookingId)
            event_create_seat.setText(event.seat)
            event_create_ref.setText(event.reference)
            event_create_notes.setText(event.note)
            event_create_type.setSelection(types.indexOf(applicationContext.getString(event.eventType.stringId)))
            subTypeId = event.eventSubType.stringId
            expenseAdapter.setExpenses(expenses)
        }
    }

    ///////////
    // Expenses
    ///////////
    //Display dialog for amount
    private fun displayAmountDialog() {
        ExpenseDialogFragment().show(fragmentManager, "new_expense1")
    }

    // After the dialog for the amount
    override fun onExpenseDialogPositiveClick(dialog: DialogFragment, amount: Float, currency: String) {
        expenseAmount = amount
        expenseCurrency = currency
        val lastDate = tripEndDay.clone() as Calendar
        lastDate.add(Calendar.MONTH, 2)
        val firstDate = tripStartDay.clone() as Calendar
        firstDate.add(Calendar.YEAR, -5)
        val dateDialogFragment =
                RangeDateDialogFragment.newInstance(
                        calendarToDate(firstDate),
                        calendarToDate(lastDate),
                        ArrayList<Date>().apply{
                            add(calendarToDate(Calendar.getInstance()))
                        }
                )
        dateDialogFragment.show(fragmentManager, "new_expense2")
    }

    // After the date for the expense
    fun addNewExpense(date: Date) {
        val expense = Expense(
                tripId = tripId,
                eventId = eventId,
                amount = expenseAmount!!,
                currency = expenseCurrency!!,
                name = null,
                date = dateToCalendar(date),
                id = UUID.randomUUID().toString()
        )
        expenses.add(expense)
        expenseAdapter.setExpenses(expenses)
    }

    // Delete Expenses
    fun deleteExpenses() {
        ExpenseDeleteDialogFragment
                .newInstance(expenses)
                .show(fragmentManager, "expenses_delete")
    }

    override fun onExpenseDeleteDialogPositiveClick(dialog: DialogFragment, expenses: ArrayList<Expense>) {
        this.expenses.removeAll(expenses)
        expenseAdapter.setExpenses(this.expenses)
    }

    // On Expense Click
    private class OnExpenseListener(): MyItemClickListener, MyItemLongClickListener {
        override fun onItemClick(view: View, position: Int) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onItemLongClick(view: View, position: Int) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

    private fun onAdressClickListener(adapterView: AdapterView<*>?, view: View?, i: Int){
        val res = adapterView!!.getItemAtPosition(i) as AutoSuggestPlace
        val coord = res.position
        res.placeDetailsRequest.execute{
            place, errorCode ->
            if (errorCode == ErrorCode.NONE){
                val address = place.location.address.text.replace("<br/>", ", ")
                event_create_address.setText(address)
                event_create_address.dismissDropDown()
                coordinate = coord
            }
        }
    }

    private fun onAdressEndClickListener(adapterView: AdapterView<*>?, view: View?, i: Int){
        val res = adapterView!!.getItemAtPosition(i) as AutoSuggestPlace
        val coord = res.position
        res.placeDetailsRequest.execute{
            place, errorCode ->
            if (errorCode == ErrorCode.NONE){
                val address = place.location.address.text.replace("<br/>", ", ")
                event_create_address_end.setText(address)
                event_create_address_end.dismissDropDown()
                coordinateEnd = coord
            }
        }
    }
}
