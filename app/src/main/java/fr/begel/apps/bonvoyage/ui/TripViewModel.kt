package fr.begel.apps.bonvoyage.ui

import android.arch.lifecycle.ViewModel
import android.content.Context
import fr.begel.apps.bonvoyage.persistence.AppRepository
import fr.begel.apps.bonvoyage.persistence.Trip
import fr.begel.apps.bonvoyage.persistence.TripDay
import java.util.*


/**
 * View Model for the [TripsList]
 */
class TripViewModel(val context: Context) : ViewModel() {

    private val mRepository = AppRepository(context)
    private var mAllTrips = mRepository.getAllTrips()
    private var mAllEvents = mRepository.getAllEvents()
    private var mAllExpenses = mRepository.getAllExpenses()
    private var mAllTripDays = mRepository.getAllTripDays()
    private var mAllEventsAndDays = mRepository.getAllEventsAndDays()

    fun getAllTrips() = mAllTrips
    fun getAllEvents() = mAllEvents
    fun getAllExpenses() = mAllExpenses
    fun getAllTripDays() = mAllTripDays
    fun getAllEventsAndDays() = mAllEventsAndDays

    fun insertAllElements(all: TripsList.AllElements) = mRepository.insertAllElements(all)

    fun insertTrip(trip: Trip) = mRepository.insertTrip(trip)
    fun deleteTrip(tripId: String) = mRepository.deleteTripById(tripId)
}
