package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import com.here.android.mpa.common.GeoCoordinate
import java.util.*

@Entity(tableName = "routes",
        foreignKeys = [
            ForeignKey(parentColumns = ["tripid"], childColumns = ["trip_id"],
                    onDelete = CASCADE, entity = Trip::class)],
        indices = [Index("trip_id")])
@TypeConverters(Converters::class)
data class RouteBtwEvents(
        @ColumnInfo(name = "trip_id") val tripId: String,
        @ColumnInfo(name = "coordinate_start") val coordinateStart: GeoCoordinate,
        @ColumnInfo(name = "coordinate_end") val coordinateEnd: GeoCoordinate,
        @ColumnInfo(name = "mode") var mode: String,
        @ColumnInfo(name = "tta") var tta: Int? = null,
        @PrimaryKey@ColumnInfo(name = "route_id") val routeId: String = UUID.randomUUID().toString()
)