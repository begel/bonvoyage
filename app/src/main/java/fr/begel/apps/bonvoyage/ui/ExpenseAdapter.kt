package fr.begel.apps.bonvoyage.ui

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import fr.begel.apps.bonvoyage.R
import fr.begel.apps.bonvoyage.listeners.*
import fr.begel.apps.bonvoyage.persistence.*
import java.util.Currency
import kotlin.math.exp

class ExpenseAdapter(private var myDataset: List<ExpenseWithName>) :
        RecyclerView.Adapter<ExpenseAdapter.ViewHolder>() {

    // Provide a reference to the views for each data item_trip
    // Complex data items may need more than one view per item_trip, and
    // you provide access to all the views for a data item_trip in a view holder.
    // Each data item_trip is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) :
            RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        val textDateView: TextView = view.findViewById(R.id.item_expense_date) as TextView
        val textNameView: TextView = view.findViewById(R.id.item_expense_title) as TextView
        val textAmountView: TextView = view.findViewById(R.id.item_expense_amount) as TextView


        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view: View) {
        }

        override fun onLongClick(view: View): Boolean {
            return true
        }
    }

    // Create new views (invoked by the view manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ExpenseAdapter.ViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_expense, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and view parameters

        return ViewHolder(layout)
    }

    // Replace the contents of a view (invoked by the view manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val expense = myDataset[position]
        holder.textDateView.text = calendarToText(expense.expense.date)
        val currency = expense.expense.amount.toString() + Currency.getInstance(expense.expense.currency).symbol
        holder.textAmountView.text = currency
        holder.textNameView.text = expense.name
    }

    // Return the size of your dataset (invoked by the view manager)
    override fun getItemCount() = myDataset.size

    fun setExpenses(expenses: List<ExpenseWithName>) {
        myDataset = expenses
        notifyDataSetChanged()
    }
}