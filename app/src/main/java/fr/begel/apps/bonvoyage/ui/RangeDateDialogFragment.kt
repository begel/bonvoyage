package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.begel.apps.bonvoyage.R
import java.util.*
import com.squareup.timessquare.CalendarPickerView
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_range_date_dialog.*
import kotlin.collections.ArrayList

const val ARG_DATE_START = "date_start"
const val ARG_DATE_END = "date_end"
const val ARG_DATES_SELECTED = "selected_dates"
class RangeDateDialogFragment : DialogFragment() {

    private var date_start: Date? = null
    private var date_end: Date? = null
    private var selected_dates : ArrayList<Date> = ArrayList()
    private var calendar: CalendarPickerView? = null
    var mListener: RangeDateDialogListener? = null

    interface RangeDateDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment, dates: MutableList<Date>);
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as RangeDateDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity.toString() + " must implement RangeDateDialogListener")
        }
    }
    
    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        //Try here
        val view = inflater!!.inflate(R.layout.fragment_range_date_dialog, null)
        calendar = view.findViewById(R.id.calendar_view) as CalendarPickerView
        //calendar = view.findViewById(R.id.calendar_view) as CalendarPickerView
        // Prepare the calendar
        arguments?.let {
            date_start = it.getSerializable(ARG_DATE_START) as Date
            date_end = it.getSerializable(ARG_DATE_END) as Date
            val tmp = it.getSerializable(ARG_DATES_SELECTED)
            selected_dates = ((tmp) ?: ArrayList<Date>()) as ArrayList<Date>
        }
        calendar!!.init(date_start, date_end!!)
                .inMode(CalendarPickerView.SelectionMode.RANGE)
                .withSelectedDates(limitsDates(selected_dates))

        val builder = AlertDialog.Builder(activity)
        // Use the Builder class for convenient dialog construction
        builder.setTitle(R.string.date_choice)
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                    val selected = calendar!!.selectedDates
                    mListener!!.onDialogPositiveClick(this, calendar!!.selectedDates)
                })
                .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                })
                .setView(view)
        // Create the AlertDialog object and return it
        return builder.create()
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param date_start Parameter 1.
         * @param date_end Parameter 2.
         * @param selected_date
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(date_start: Date, date_end: Date, selected_dates: ArrayList<Date>) =
                RangeDateDialogFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_DATE_START, date_start)
                        putSerializable(ARG_DATE_END, date_end)
                        putSerializable(ARG_DATES_SELECTED, selected_dates)
                    }
                }
    }
}
