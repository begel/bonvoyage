package fr.begel.apps.bonvoyage.persistence

import android.arch.persistence.room.TypeConverter
import com.here.android.mpa.common.GeoCoordinate
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Calendar? {
        if (value == null)
            return null
        else{
            val calendar = GregorianCalendar()
            calendar.timeInMillis = value
            return calendar
        }
    }

    @TypeConverter
    fun dateToTimestamp(date: Calendar?): Long? {
        return date?.timeInMillis
    }

    @TypeConverter
    fun eventTypeToInt(eventType: EventType?): Int? {
        if (eventType == null)
            return null
        return eventType.id
    }

    @TypeConverter
    fun intToEventType(int: Int?): EventType? {
        for (type in EventType.values()){
            if (type.id == int) return type
        }
        return null
    }

    @TypeConverter
    fun eventSubTypeToInt(eventSubType: EventSubType?): Int? {
        if (eventSubType == null)
            return null
        return eventSubType.id
    }

    @TypeConverter
    fun intToEventSubType(int: Int?): EventSubType? {
        for (type in EventSubType.values()){
            if (type.id == int) return type
        }
        return null
    }

    @TypeConverter
    fun geoCoordinateToPair(geoCoordinate: GeoCoordinate?): String?{
        if (geoCoordinate == null) return null
        return "${geoCoordinate.latitude},${geoCoordinate.longitude}"
    }

    @TypeConverter
    fun pairToGeoCoordinate(string: String?): GeoCoordinate?{
        if (string == null) return null
        val coordinate = string.split(",")
        return GeoCoordinate(coordinate.first().toDouble(), coordinate.last().toDouble())
    }
}