package fr.begel.apps.bonvoyage.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

/**
 * Data Access Object for the event table.
 */
@Dao
interface EventDAO {

    /**
     * Get the events of a trip

     * @return the event from the table with a tripId
     */
    @Query("SELECT * FROM Events WHERE trip_id = :id")
    fun getEventsOfTrip(id: String): LiveData<List<Event>>

    @Query("SELECT * FROM events WHERE id IN (:eventsId)")
    fun getEventsById(eventsId: List<String>): LiveData<List<Event>>

    @Query("SELECT * FROM events")
    fun getAllEvents(): LiveData<List<Event>>

    /**
     * Insert a event in the database. If the event already exists, replace it.

     * @param event the event to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEvent(event: Event)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllEvents(events: List<Event>)

    /**
     * Delete one event by id.
     * @param eventid the eventid to be deleted
     */
    @Query("DELETE FROM events where id = :id")
    fun deleteEventById(id: String)
}