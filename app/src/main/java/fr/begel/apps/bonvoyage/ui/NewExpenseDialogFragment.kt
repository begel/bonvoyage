package fr.begel.apps.bonvoyage.ui

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.*
import fr.begel.apps.bonvoyage.R
import kotlin.collections.ArrayList

const val SPINNER_DIALOG_CHOICES = "fr.begel.apps.bonvoyage.ui.spinner_dialog_choices"
class NewExpenseDialogFragment() : DialogFragment() {

    private lateinit var checkBox: CheckBox
    private lateinit var spinner: Spinner
    private lateinit var editText: EditText
    private lateinit var choices: ArrayList<Pair<String, String>>
    var mListener: NewExpenseDialogListener? = null

    // Interface to link the implemented function to the mListener
    interface NewExpenseDialogListener {
        fun newExpenseDialogPositiveClick(dialog: DialogFragment, eventId: String?, name: String);
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = activity as NewExpenseDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(activity.toString() + " must implement NewExpenseDialogListener")
        }
    }
    
    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    // OnCreate
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        // Argument
        arguments?.let {
            choices = it.getSerializable(SPINNER_DIALOG_CHOICES) as ArrayList<Pair<String, String>>
        }
        //Component
        val view = inflater!!.inflate(R.layout.fragment_spinner_dialog, null)
        checkBox = view.findViewById(R.id.checkBox_spinner_dialog)
        spinner = view.findViewById(R.id.spinner_dialog)
        editText = view.findViewById(R.id.text_spinner_dialog)
        // Spinner - fill with choices
        val dataAdapterType = ArrayAdapter(view.context,
                android.R.layout.simple_spinner_item,
                choices.map { element -> element.second })
        dataAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.setAdapter(dataAdapterType)
        // Listener checkBox
        checkBox.setOnClickListener { _ ->
            if (checkBox.isChecked) {
                spinner.visibility = View.VISIBLE
            } else {
                spinner.visibility = View.INVISIBLE
            }
        }
        //Builder
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Nouvelle dépense")
                .setPositiveButton(R.string.next) { dialog, id ->
                    // choice is the ID of the selected Item
                    val choice = if (!checkBox.isChecked)
                        null
                    else
                        choices[spinner.selectedItemPosition].first
                    mListener!!.newExpenseDialogPositiveClick(this, choice, editText.text.toString())
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                    // User cancelled the dialog
                    dialog.cancel()
                }
                .setView(view)
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(choices: ArrayList<Pair<String, String>>) =
                NewExpenseDialogFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(SPINNER_DIALOG_CHOICES, choices)
                    }
                }
    }
}
